import pytest

from pmc.board import Board
from pmc.move_generator import BishopMoveGenerator, KingMoveGenerator, KnightMoveGenerator, QueenMoveGenerator, RookMoveGenerator


def test_knight_moves(benchmark):
    # taken from unit tests
    board = Board(fen="8/K7/8/8/3N4/5p2/8/7k w - - 0 1")

    gen = KnightMoveGenerator.generate
    result = benchmark(gen, board, "d4", "w")
    assert result


def test_rook_moves(benchmark):
    # taken from unit tests
    board = Board(fen="R7/K7/8/8/8/4p3/8/7k w - - 0 1")
    gen = RookMoveGenerator.generate

    result = benchmark(gen, board, "a8", "w")
    assert result


def test_bishop_moves(benchmark):
    # taken from unit tests
    board = Board(fen="rnbqkbnr/pp2pppp/8/2pP4/3P4/8/PP2PPPP/RNBQKBNR b KQkq - 0 3")
    gen = BishopMoveGenerator.generate

    result = benchmark(gen, board, "c1", "w")
    assert result


@pytest.mark.slow
def test_queen_moves(benchmark):
    # taken from unit tests
    board = Board(fen="K7/8/8/1r4n1/8/4Q3/8/2N4k w - - 0 1")
    gen = QueenMoveGenerator.generate

    result = benchmark(gen, board, "e3", "w")
    assert result


def test_king_moves(benchmark):
    # taken from unit tests
    board = Board(fen="8/2PKp3/8/1r2R2n/8/4p3/8/7k w - - 0 1")
    gen = KingMoveGenerator.generate

    result = benchmark(gen, board, "d7", "w")
    assert result
