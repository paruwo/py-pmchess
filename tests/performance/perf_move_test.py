import pytest

from pmc.move import Move, index_to_position, position_to_index
from pmc.move_generator import chess_distance, invalid_position


def test_chess_distance(benchmark):
    result = benchmark(chess_distance, "a2", "h8")
    assert result


def test_invalid_position_negative(benchmark):
    result = benchmark(invalid_position, "a1", "h2")
    assert result


def test_invalid_position_positive(benchmark):
    result = benchmark(invalid_position, "a2", "b2")
    assert not result


def test_position_to_index(benchmark):
    benchmark(position_to_index, "a2")


def test_index_to_position(benchmark):
    benchmark(index_to_position, 42)


def test_rank_from_position(benchmark):
    benchmark(Move.rank_from_position, "a1")


def test_file_from_position(benchmark):
    benchmark(Move.file_from_position, "h8")


def test_move_constructor(benchmark):
    benchmark(Move, "a3", "w")
