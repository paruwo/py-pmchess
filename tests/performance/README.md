
# Overview

I am fully aware that this will never become the fastest/best
chess engine for many reasons, such as it's a fun project only,
no copy-paste from other engines as well as all the Python's
limitations like running in an interpreted environment,
having GIL in place, etc.

Nevertheless, it's also a joy to step into performance topic
with all the known constraints. :)

## Overall perft tests

To trace the overall move generation performance, use this call
and mind the column "tottime":

    python3 -m cProfile -s tottime pmc/perft.py

As the result os sorted by total execution time, we can start to
optimize from top to down.

## Performance test suite

To call specific performance tests, run this command:

    cd tests/performance
    pytest --benchmark-autosave \
           --benchmark-compare \
           --benchmark-disable-gc \
           --benchmark-name=short \
           --benchmark-sort=name \
           .


# Ideas to improve Python performance

### First Ideas using Tracing Results

* move.py:168(\_\_init\_\_)
  * split and investigate in detail
* BishopMoveGenerator.generate
  * ray attack, bitboard
* RookMoveGenerator.generate
  * ray attack, bitboard
* KnightMoveGenerator.generate
  * precompute
* DEBUG logging
* generate_possible_capture_moves


### General Ideas

* deque instead of lists for volatile structures (better complexity, less predictive access)
* use bitboards together with native numpy data types and vectorization
* where possible evaluate GPU computing
