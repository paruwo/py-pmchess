import pytest

from pmc.board import Board
from pmc.perft import Perft


@pytest.fixture
def perft():
    return Perft()


def test_perft1(perft):
    result, _ = perft._run(depth=1, board=Board(), initial_depth=1, divide=False)
    assert result.nodes == 20
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0

    result, _ = perft._run(depth=1, board=Board(), initial_depth=1, divide=True)
    assert result.nodes == 20
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0

    result = perft._run_bulk(depth=1, board=Board(), initial_depth=1, divide=False)
    assert result.nodes == 20
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0


def test_perft2(perft):
    result, _ = perft._run(depth=2, board=Board(), initial_depth=2, divide=False)
    assert result.nodes == 400
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0

    result, _ = perft._run(depth=2, board=Board(), initial_depth=2, divide=False)
    assert result.nodes == 400
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0

    result = perft._run_bulk(depth=2, board=Board(), initial_depth=2, divide=True)
    assert result.nodes == 400
    assert result.captures == 0
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    assert result.checks == 0


def test_perft3(perft):
    result, div_map = perft._run(depth=3, board=Board(), initial_depth=3, divide=True)

    assert result.nodes == 8902
    assert result.captures == 34
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    # TODO FIXME
    # assert result.checks == 12

    # reference is stockfish "go perft 3"
    assert div_map["a3"] == 380
    assert div_map["b3"] == 420
    assert div_map["c3"] == 420
    assert div_map["d3"] == 539
    assert div_map["e3"] == 599
    assert div_map["f3"] == 380
    assert div_map["g3"] == 420
    assert div_map["h3"] == 380

    assert div_map["a4"] == 420
    assert div_map["b4"] == 421
    assert div_map["c4"] == 441
    assert div_map["d4"] == 560
    assert div_map["e4"] == 600
    assert div_map["f4"] == 401
    assert div_map["g4"] == 421
    assert div_map["h4"] == 420

    assert div_map["Nb1a3"] == 400
    assert div_map["Nb1c3"] == 440
    assert div_map["Ng1f3"] == 440
    assert div_map["Ng1h3"] == 400


@pytest.mark.slow
def test_perft4(perft):
    # Takes 10+ seconds
    result, div_map = perft._run(depth=4, board=Board(), initial_depth=4, divide=True)

    # FIXME
    # assert result.nodes == 197281  # too many
    # assert result.captures == 1576  # too many
    assert result.castles == 0
    assert result.ep_captures == 0
    assert result.promotions == 0
    # assert result.checks == 469
    # assert result.checkmate == 8

    # reference is stockfish "go perft 4"
    assert div_map["a3"] == 8457
    assert div_map["b3"] == 9345
    # assert div_map['c3'] == 9272  # too many
    assert div_map["d3"] == 11959
    # assert div_map['e3'] == 13134  # too many
    assert div_map["f3"] == 8457
    assert div_map["g3"] == 9345
    assert div_map["h3"] == 8457

    assert div_map["a4"] == 9329
    assert div_map["b4"] == 9332
    # assert div_map['c4'] == 9744  # too many
    assert div_map["d4"] == 12435
    # assert div_map['e4'] == 13160  # too many
    assert div_map["f4"] == 8929
    assert div_map["g4"] == 9328
    assert div_map["h4"] == 9329

    assert div_map["Nb1a3"] == 8885
    assert div_map["Nb1c3"] == 9755
    assert div_map["Ng1f3"] == 9748
    assert div_map["Ng1h3"] == 8881
