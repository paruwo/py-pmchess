from pmc.board import Board
from pmc.move_generator import QueenMoveGenerator


def test_sliding_moves():
    board = Board(fen="K7/8/8/1r4n1/8/4Q3/8/2N4k w - - 0 1")
    moves = QueenMoveGenerator.generate(board=board, position="e3", color="w")

    assert len(moves) == 23

    # N
    assert "Qe3e4" in [m.san for m in moves]
    assert "Qe3e5" in [m.san for m in moves]
    assert "Qe3e6" in [m.san for m in moves]
    assert "Qe3e7" in [m.san for m in moves]
    assert "Qe3e8" in [m.san for m in moves]
    # E
    assert "Qe3f3" in [m.san for m in moves]
    assert "Qe3g3" in [m.san for m in moves]
    assert "Qe3h3" in [m.san for m in moves]
    # S
    assert "Qe3e2" in [m.san for m in moves]
    assert "Qe3e1" in [m.san for m in moves]
    # W
    assert "Qe3d3" in [m.san for m in moves]
    assert "Qe3c3" in [m.san for m in moves]
    assert "Qe3b3" in [m.san for m in moves]
    assert "Qe3a3" in [m.san for m in moves]

    # NE
    assert "Qe3f4" in [m.san for m in moves]
    assert "Qe3xg5" in [m.san for m in moves]
    # SE
    assert "Qe3f2" in [m.san for m in moves]
    assert "Qe3g1" in [m.san for m in moves]
    # SW
    assert "Qe3d2" in [m.san for m in moves]
    # NW
    assert "Qe3d4" in [m.san for m in moves]
    assert "Qe3c5" in [m.san for m in moves]
    assert "Qe3b6" in [m.san for m in moves]
    assert "Qe3a7" in [m.san for m in moves]

    # same for black
    board = Board(fen="K7/8/8/1r4n1/8/4Q3/8/2N4k w - - 0 1")
    board.flip()
    moves = QueenMoveGenerator.generate(board=board, position="e6", color="b")
    assert len(moves) == 23
