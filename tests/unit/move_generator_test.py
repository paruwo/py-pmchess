from pmc.board import Board
from pmc.move_generator import GenericMoveGenerator, MoveGenerator, chess_distance, invalid_position


def test_is_own_color():
    assert GenericMoveGenerator.is_own_color(piece="P", color="w")
    assert not GenericMoveGenerator.is_own_color(piece="p", color="w")

    assert GenericMoveGenerator.is_own_color(piece="p", color="b")
    assert not GenericMoveGenerator.is_own_color(piece="P", color="b")

    assert not GenericMoveGenerator.is_own_color(piece=".", color="w")
    assert not GenericMoveGenerator.is_own_color(piece=".", color="b")


def test_is_opposite_color():
    assert GenericMoveGenerator.is_opposite_color(piece="P", color="b")
    assert not GenericMoveGenerator.is_opposite_color(piece="p", color="b")

    assert GenericMoveGenerator.is_opposite_color(piece="p", color="w")
    assert not GenericMoveGenerator.is_opposite_color(piece="P", color="w")

    assert not GenericMoveGenerator.is_opposite_color(piece=".", color="w")
    assert not GenericMoveGenerator.is_opposite_color(piece=".", color="b")


def test_is_opponent():
    board = Board()
    assert GenericMoveGenerator.is_opponent(board=board, target_position="a7", reference_color="w")
    assert GenericMoveGenerator.is_opponent(board=board, target_position="h7", reference_color="w")
    assert GenericMoveGenerator.is_opponent(board=board, target_position="c8", reference_color="w")
    assert not GenericMoveGenerator.is_opponent(board=board, target_position="b5", reference_color="w")

    assert GenericMoveGenerator.is_opponent(board=board, target_position="a2", reference_color="b")
    assert GenericMoveGenerator.is_opponent(board=board, target_position="h2", reference_color="b")
    assert GenericMoveGenerator.is_opponent(board=board, target_position="c1", reference_color="b")
    assert not GenericMoveGenerator.is_opponent(board=board, target_position="b5", reference_color="b")


def test_invalid_positions():
    assert invalid_position(position="a0")
    assert invalid_position(position="a9")
    assert invalid_position(position="h0")
    assert invalid_position(position="h9")


def test_generate_all_first_moves():
    board = Board()
    mg = MoveGenerator(setup_board=board)
    mg.generate()
    assert len(mg.moves) == 20
    assert len(mg.moves) == 20


def test_challenge_kiwipete():
    # https://www.chessprogramming.org/Perft_Results#Position_2
    board = Board(fen="r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -")
    mg = MoveGenerator(setup_board=board)
    mg.generate()

    assert len(mg.moves) == 48
    assert mg.get_num_castlings() == 2
    assert mg.get_num_captures() == 8
    # assert mg.get_num_promotions() == 0  # FIXME


def test_make_move_side():
    board = Board()
    mg = MoveGenerator(setup_board=board)
    mg.generate()

    # all first white moves
    assert len(mg.moves) == 20
    assert all([m.color == "w" for m in mg.moves])

    # white moves one piece
    board.make_move(mg.moves.pop())

    # all first black moves
    mg2 = MoveGenerator(setup_board=board)
    mg2.generate()
    assert all([m.color == "b" for m in mg2.moves])


def test_chess_distance():
    assert chess_distance(position1="f6", position2="c4") == 3
    assert chess_distance(position1="c4", position2="f6") == 3

    assert chess_distance(position1="a1", position2="h8") == 7
    assert chess_distance(position1="h8", position2="a1") == 7

    assert chess_distance(position1="f6", position2="g7") == 1
    assert chess_distance(position1="g7", position2="f6") == 1

    assert chess_distance(position1="h1", position2="g7") == 6
    assert chess_distance(position1="g7", position2="h1") == 6
