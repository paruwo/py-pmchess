import pytest

from pmc.fen import Position


def test_easy():
    # sum = 8 for all ranks
    fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")
    assert fen_pos.validate()


def test_invalid():
    with pytest.raises(AttributeError):
        # sum = 8 for all ranks (7)
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/7/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # consecutive numbers (44)
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/44/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # two white kings
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQKKNR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # no white kings
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQ2NR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # no black kings
        fen_pos = Position(fen="rnb2bnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")

    # with pytest.raises(AttributeError):
    #   black and white king next to each other
    #   fen_pos = FenPosition(fen='rnbKkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNB2BNR b KQkq - 0 1')

    with pytest.raises(AttributeError):
        # too many white pawns
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3PP3/PPP1PPPP/RNBQKBNR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # too many black pawns
        fen_pos = Position(fen="rnbqkbnr/ppppp1pp/5pp1/8/8/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")

    with pytest.raises(AttributeError):
        # white unpromoted pawn
        fen_pos = Position(fen="k6P/p7/8/8/8/8/P7/K7 b - - 0 1")

    with pytest.raises(AttributeError):
        # black unpromoted pawn
        fen_pos = Position(fen="k7/p7/8/8/8/8/P7/K6p b - - 0 1")

    with pytest.raises(AttributeError):
        # white pawn in invalid rank
        fen_pos = Position(fen="k7/p7/8/8/8/8/8/KP6 b - - 0 1")

    with pytest.raises(AttributeError):
        # black unpromoted pawn
        fen_pos = Position(fen="kp6/8/8/8/8/8/P7/K7 w - - 0 1")
