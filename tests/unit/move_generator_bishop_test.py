from pmc.board import Board
from pmc.move_generator import BishopMoveGenerator


def test_no_moves():
    board = Board()
    moves = BishopMoveGenerator.generate(board=board, position="c8", color="b")
    assert len(moves) == 0

    moves = BishopMoveGenerator.generate(board=board, position="f8", color="b")
    assert len(moves) == 0


def test_basic_moves():
    board = Board(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")
    moves = BishopMoveGenerator.generate(board=board, position="c1", color="w")
    assert len(moves) == 5

    board = Board(fen="rnbqkbnr/ppppp1pp/5p2/8/8/3P4/PPP1PPPP/RNBQKBNR b KQkq - 0 1")
    moves = BishopMoveGenerator.generate(board=board, position="c1", color="w")
    assert len(moves) == 5


def test_sliding_moves():
    board = Board(fen="K7/8/8/1r4n1/8/4B3/8/2N4k w - - 0 1")
    moves = BishopMoveGenerator.generate(board=board, position="e3", color="w")

    assert len(moves) == 9
    # NE
    assert "Be3f4" in [m.san for m in moves]
    assert "Be3xg5" in [m.san for m in moves]
    # SE
    assert "Be3f2" in [m.san for m in moves]
    assert "Be3g1" in [m.san for m in moves]
    # SW
    assert "Be3d2" in [m.san for m in moves]
    # NW
    assert "Be3d4" in [m.san for m in moves]
    assert "Be3c5" in [m.san for m in moves]
    assert "Be3b6" in [m.san for m in moves]
    assert "Be3a7" in [m.san for m in moves]

    # same in black
    board = Board(fen="K7/8/8/1r4n1/8/4B3/8/2N4k w - - 0 1")
    board.flip()
    moves = BishopMoveGenerator.generate(board=board, position="e6", color="b")
    assert len(moves) == 9


def test_moves_crossing_borders():
    board = Board(fen="rnbqkbnr/pp2pppp/8/2pP4/3P4/8/PP2PPPP/RNBQKBNR b KQkq - 0 3")
    moves = BishopMoveGenerator.generate(board=board, position="c1", color="w")
    assert len(moves) == 5

    # same in black
    board = Board(fen="rnbqkbnr/pp2pppp/8/2pP4/3P4/8/PP2PPPP/RNBQKBNR b KQkq - 0 3")
    board.flip()
    moves = BishopMoveGenerator.generate(board=board, position="c8", color="b")
    assert len(moves) == 5


def test_crude_situation():
    board = Board(fen="8/B4k1r/8/8/8/5P1p/PPn1P2P/5K1R w - - 2 27")
    moves = BishopMoveGenerator.generate(board=board, position="a7", color="w")
    assert len(moves) == 7

    # same in black
    board = Board(fen="8/B4k1r/8/8/8/5P1p/PPn1P2P/5K1R w - - 2 27")
    board.flip()
    moves = BishopMoveGenerator.generate(board=board, position="a2", color="b")
    assert len(moves) == 7
