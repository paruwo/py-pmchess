import pytest

from pmc.move import Move, index_to_position


def test_pawn_push():
    # a8, a4, A8Q
    ma8 = Move(san="a8", color="w")
    assert ma8.piece == "P"
    assert ma8.from_position == "a"
    assert ma8.to_position == "a8"
    assert ma8.promotion_piece is None
    assert ma8.from_index == -1
    assert ma8.to_index > -1
    assert ma8.move_type == Move.PAWN_PUSH
    assert ma8.is_capture is False
    assert ma8.captured_piece == "."

    ma3 = Move(san="a4", color="w")
    assert ma3.piece == "P"
    assert ma3.from_position == "a"
    assert ma3.to_position == "a4"
    assert ma3.promotion_piece is None
    assert ma3.from_index == -1
    assert ma3.to_index > -1
    assert ma3.move_type == Move.PAWN_PUSH
    assert ma3.is_capture is False
    assert ma3.captured_piece == "."

    m8q = Move(san="A8Q", color="w")
    assert m8q.piece == "P"
    assert m8q.from_position == "a"
    assert m8q.to_position == "a8"
    assert m8q.promotion_piece == "Q"
    assert m8q.from_index == -1
    assert m8q.to_index > -1
    assert m8q.move_type == Move.PAWN_PUSH
    assert m8q.is_capture is False
    assert m8q.captured_piece == "."


def test_pawn_capture():
    # axb5, a4xb5, axb8q, a7xb8q
    mb5 = Move(san="axb5", color="w")
    mb5.set_captured_piece("p")
    assert mb5.piece == "P"
    assert mb5.from_position == "a"
    assert mb5.to_position == "b5"
    assert mb5.promotion_piece is None
    assert mb5.from_index == -1
    assert mb5.to_index > -1
    assert mb5.move_type == Move.PAWN_CAPTURE
    assert mb5.is_capture is True
    assert mb5.captured_piece == "p"

    mb5_2 = Move(san="a4xb5", color="w")
    assert mb5_2.piece == "P"
    assert mb5_2.from_position == "a4"
    assert mb5_2.to_position == "b5"
    assert mb5_2.promotion_piece is None
    assert mb5_2.from_index > -1
    assert mb5_2.to_index > -1
    assert mb5_2.move_type == Move.PAWN_CAPTURE
    assert mb5_2.is_capture is True

    mb8q = Move(san="axb8q", color="w")
    assert mb8q.piece == "P"
    assert mb8q.from_position == "a"
    assert mb8q.to_position == "b8"
    assert mb8q.promotion_piece == "Q"
    assert mb8q.from_index == -1
    assert mb8q.to_index > -1
    assert mb8q.move_type == Move.PAWN_CAPTURE
    assert mb8q.is_capture is True

    mb8q_2 = Move(san="a7xb8q", color="w")
    assert mb8q_2.piece == "P"
    assert mb8q_2.from_position == "a7"
    assert mb8q_2.to_position == "b8"
    assert mb8q_2.promotion_piece == "Q"
    assert mb8q_2.from_index > -1
    assert mb8q_2.to_index > -1
    assert mb8q_2.move_type == Move.PAWN_CAPTURE
    assert mb8q_2.is_capture is True


def test_position_to_index():
    move_top_left = Move(san="a8", color="w")
    assert move_top_left.from_index == -1
    assert move_top_left.to_index == 0

    move_top_right = Move(san="h8", color="w")
    assert move_top_right.from_index == -1
    assert move_top_right.to_index == 7

    move_bottom_left = Move(san="b1", color="w")
    assert move_bottom_left.from_index == -1
    assert move_bottom_left.to_index == 57

    move_bottom_right = Move(san="h1", color="w")
    assert move_bottom_right.from_index == -1
    assert move_bottom_right.to_index == 63


def test_index_to_position():
    m = Move(san="a8", color="w")
    assert index_to_position(index=m.to_index) == "a8"

    m = Move(san="a1", color="w")
    assert index_to_position(index=m.to_index) == "a1"

    m = Move(san="b5", color="w")
    assert index_to_position(index=m.to_index) == "b5"


def test_rank_from_position():
    assert Move.rank_from_position(position="a1") == 1
    assert Move.rank_from_position(position="a8") == 8
    assert Move.rank_from_position(position="h1") == 1
    assert Move.rank_from_position(position="h8") == 8


def test_file_from_position():
    assert Move.file_from_position(position="a1") == 0
    assert Move.file_from_position(position="a8") == 0
    assert Move.file_from_position(position="h1") == 7
    assert Move.file_from_position(position="h8") == 7


def test_move_from_pure():
    m = Move.from_pure(pure="e3f4", color="w", piece="B", capture=False)
    assert m.san == "Be3f4"

    m = Move.from_pure(pure="e3f4", color="w", piece="", capture=True)
    assert m.san == "e3xf4"

    m = Move.from_pure(pure="f7f8Q", color="w", piece="P", capture=False)
    assert m.san == "f8Q"

    m = Move.from_pure(pure="e7f8Q", color="w", piece="P", capture=True)
    assert m.san == "e7xf8Q"


def test_to_pure():
    # pawn
    m = Move.from_pure(pure="e3f4", color="w", piece="B", capture=False)
    assert m.to_pure() == "e3f4"

    m = Move(san="axb5", color="w")
    m.set_from_position(from_position="a4")
    assert m.to_pure() == "a4b5"

    m = Move(san="axb8Q", color="w")
    m.set_from_position(from_position="a7")
    assert m.to_pure() == "a7b8Q"

    # king
    m = Move(san="Kd7c8", color="w")
    assert m.to_pure() == "d7c8"

    m = Move(san="Nb1a3", color="w")
    assert m.to_pure() == "b1a3"


def test_to_lan():
    m = Move.from_pure(pure="e3f4", color="w", piece="B", capture=False)
    assert m.to_lan() == "Be3-f4"

    m = Move(san="axb5", color="w")
    m.set_from_position(from_position="a4")
    assert m.to_lan() == "a4xb5"

    m = Move(san="A8Q", color="w")
    m.set_from_position(from_position="a7")
    assert m.to_lan() == "a7-a8Q"

    m = Move(san="axb8q", color="w")
    m.set_from_position(from_position="a7")
    assert m.to_lan() == "a7xb8Q"


def test_from_lan():
    m = Move.from_lan(lan="Rxe6", color="w", from_position="e1")
    assert m.to_pure() == "e1e6"
    assert m.to_lan() == "Re1xe6"
    assert m.piece == "R"

    m = Move.from_lan(lan="Ng4!", color="b", from_position="f2")
    assert m.to_pure() == "f2g4"
    assert m.to_lan() == "Nf2-g4"
    assert m.piece == "n"

    m = Move.from_lan(lan="Qxh6??", color="w", from_position="c6")
    assert m.to_pure() == "c6h6"
    assert m.to_lan() == "Qc6xh6"
    assert m.piece == "Q"

    m = Move.from_lan(lan="Bd4+", color="b", from_position="e5")
    assert m.to_pure() == "e5d4"
    assert m.to_lan() == "Be5-d4"
    assert m.piece == "b"

    # pawn promotion
    m = Move.from_lan(lan="e8Q", color="w", from_position="e7")
    print(m)
    assert m.to_pure() == "e7e8Q"
    assert m.to_lan() == "e7-e8Q"
    assert m.piece == "P"


def test_invalid_move():
    with pytest.raises(AttributeError):
        Move(san="a2a4", color="w")


def test_is_sliding_piece():
    assert Move.is_sliding_piece(piece="b") is True
    assert Move.is_sliding_piece(piece="B") is True
    assert Move.is_sliding_piece(piece="r") is True
    assert Move.is_sliding_piece(piece="R") is True
    assert Move.is_sliding_piece(piece="q") is True
    assert Move.is_sliding_piece(piece="Q") is True


def test_to_epd():
    m = Move(san="a3", color="w")
    m.set_from_position(from_position="a2")
    assert m.to_epd() == "a2a3"

    m = Move(san="a3xb4", color="w")
    assert m.to_epd() == "a3xb4"
