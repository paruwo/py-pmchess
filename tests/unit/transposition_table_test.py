from pmc.transposition_table import TranspositionTable


def test_put():
    tt = TranspositionTable(capacity_size=3)
    assert tt.capacity == 3
    assert tt.size == 0

    tt.put(hash_value=1, best_move="a2a3")
    tt.put(hash_value=2, best_move="b2b3")
    tt.put(hash_value=3, best_move="c2c3")

    assert tt.capacity == 3
    assert tt.size == 3

    assert str(tt.cache.keys()) == "odict_keys([1, 2, 3])"


def test_get():
    tt = TranspositionTable(capacity_size=10)
    assert tt.capacity == 10
    assert tt.size == 0
    tt.put(hash_value=1, best_move="a2a3")
    tt.put(hash_value=2, best_move="b2b3")
    tt.put(hash_value=3, best_move="c2c3")
    assert tt.capacity == 10
    assert tt.size == 3

    # 3 is last
    assert str(tt.cache.keys()) == "odict_keys([1, 2, 3])"

    tt.get(hash_value=1)
    # 1 should be last
    assert str(tt.cache.keys()) == "odict_keys([2, 3, 1])"


def test_setup_mb():
    tt = TranspositionTable(capacity_mb=1)
    assert tt.capacity == 6850
    assert tt.size == 0


def test_empty_get():
    tt = TranspositionTable(capacity_size=10)
    assert tt.get(hash_value=-1) is None


def test_replacement():
    tt = TranspositionTable(capacity_size=2)

    tt.put(hash_value=1, best_move="a3")
    tt.put(hash_value=2, best_move="a6")
    assert tt.capacity == 2
    assert tt.size == 2

    tt.put(hash_value=3, best_move="b3")
    assert tt.capacity == 2
    assert tt.size == 2


def test_fill_state():
    tt = TranspositionTable(capacity_size=4)
    assert tt.get_fill_state() == 0

    tt.put(hash_value=1, best_move="a3")
    assert tt.get_fill_state() == 250

    tt.put(hash_value=1, best_move="a6")
    assert tt.get_fill_state() == 500

    tt.put(hash_value=1, best_move="b3")
    assert tt.get_fill_state() == 750

    tt.put(hash_value=1, best_move="b6")
    assert tt.get_fill_state() == 1000


def test_empty():
    tt = TranspositionTable(capacity_size=0)
    assert tt.get_fill_state() == 0
    assert tt.get(hash_value=-1) is None

    tt.put(hash_value=1, best_move="a3")
    assert tt.get(hash_value=1) is None
    assert tt.get_fill_state() == 0
