import pytest

from pmc.config import Config
from pmc.epd import Epd


def test_epd_not_exists():
    not_existing = Config().root_path / "resource" / "testsuites" / "not_existing.epd"
    with pytest.raises(AttributeError):
        Epd(file_path=not_existing)


def test_epd_exists():
    kaufman_epd = Config().root_path / "resource" / "testsuites" / "kaufman.epd"
    epd = Epd(file_path=kaufman_epd)
    assert epd is not None
