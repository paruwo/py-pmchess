from pmc.board import Board
from pmc.move import Move
from pmc.move_generator import PawnMoveGenerator


def test_single_push():
    board = Board(fen="7k/8/8/8/8/8/P7/K7 w - - 0 1")
    moves = PawnMoveGenerator.generate(board=board, position="a2", color="w")
    assert len(moves) == 2
    assert "a3" in [m.san for m in moves]
    assert "a4" in [m.san for m in moves]
    assert not any([m.is_capture for m in moves])

    # same in black
    board = Board(fen="7k/8/8/8/8/8/P7/K7 w - - 0 1")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="a7", color="b")
    assert len(moves) == 2
    assert "a6" in [m.san for m in moves]
    assert not any([m.is_capture for m in moves])


def test_double_push():
    board = Board(fen="7k/8/8/8/8/8/P7/K7 w - - 0 1")
    moves = PawnMoveGenerator.generate(board=board, position="a2", color="w")
    assert len(moves) == 2
    assert "a4" in [m.san for m in moves]
    assert any(m for m in moves if m.is_double_pawn_push)
    assert not any([m.is_capture for m in moves])

    # same in black
    board = Board(fen="7k/8/8/8/8/8/P7/K7 w - - 0 1")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="a7", color="b")
    assert len(moves) == 2
    assert "a5" in [m.san for m in moves]
    assert any(m for m in moves if m.is_double_pawn_push)
    assert not any([m.is_capture for m in moves])


def test_capture_easy():
    board = Board(fen="7k/8/8/2rbn3/3P4/8/8/K7 w KQkq -")
    moves = PawnMoveGenerator.generate(board=board, position="d4", color="w")
    assert len(moves) == 2
    assert sum([1 for m in moves if m.is_capture]) == 2
    assert "d4xc5" in [m.san for m in moves]
    assert "d4xe5" in [m.san for m in moves]

    # back to black
    board = Board(fen="k7/8/8/2rbn3/3P4/8/8/7K w KQkq -")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="d5", color="b")
    assert len(moves) == 2
    assert sum([1 for m in moves if m.is_capture]) == 2
    assert "d5xc4" in [m.san for m in moves]
    assert "d5xe4" in [m.san for m in moves]


def test_capture_no_flipover():
    board = Board(fen="k7/8/8/rb5n/P7/8/8/K7 w KQkq -")
    moves = PawnMoveGenerator.generate(board=board, position="a4", color="w")
    assert len(moves) == 1
    assert sum([1 for m in moves if m.is_capture]) == 1
    assert "a4xb5" in [m.san for m in moves]

    # same in black
    board = Board(fen="k7/8/8/rb5n/P7/8/8/K7 w KQkq -")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="a5", color="b")
    assert len(moves) == 1
    assert sum([1 for m in moves if m.is_capture]) == 1
    assert "a5xb4" in [m.san for m in moves]


def test_promotion():
    board = Board(fen="7k/P7/8/8/8/8/8/7K w - - 0 1")
    moves = PawnMoveGenerator.generate(board=board, position="a7", color="w")
    assert len(moves) == 4
    assert sum([1 for m in moves if m.is_capture]) == 0
    assert "a8Q" in [m.san for m in moves]
    assert "a8R" in [m.san for m in moves]
    assert "a8B" in [m.san for m in moves]
    assert "a8N" in [m.san for m in moves]

    # black again
    board = Board(fen="7k/P7/8/8/8/8/8/7K w - - 0 1")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="a2", color="b")
    assert len(moves) == 4
    assert sum([1 for m in moves if m.is_capture]) == 0
    assert "a1q" in [m.san for m in moves]
    assert "a1r" in [m.san for m in moves]
    assert "a1b" in [m.san for m in moves]
    assert "a1n" in [m.san for m in moves]


def test_en_passant_capture():
    board = Board(fen="7k/8/8/Pp6/8/8/8/K7 w - b6 0 1")
    moves = PawnMoveGenerator.generate(board=board, position="a5", color="w")
    assert len(moves) == 2

    board = Board(fen="7k/8/8/6Pp/8/8/8/K7 w - h6 0 1")
    moves = PawnMoveGenerator.generate(board=board, position="g5", color="w")
    assert len(moves) == 2

    # black
    board = Board(fen="7k/8/8/Pp6/8/8/8/K7 w - b6 0 1")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="a4", color="b")
    assert len(moves) == 2

    board = Board(fen="7k/8/8/6Pp/8/8/8/K7 w - h6 0 1")
    board.flip()
    moves = PawnMoveGenerator.generate(board=board, position="g4", color="b")
    assert len(moves) == 2


def test_pawn_lookup():
    board = Board()
    board.make_move(Move(san="a3", color="w"))
    board.make_move(Move(san="a6", color="b"))
