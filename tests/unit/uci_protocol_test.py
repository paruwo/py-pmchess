from pmc.board import Board
from pmc.client import UCI, Engine


def test_init(capsys):
    engine = Engine(protocol="uci")
    uci = UCI(engine=engine)
    uci.do_uci(line="")

    captured = capsys.readouterr().out
    assert captured == "id name pmc 0.1\nid author paruwo\nuciok\n"

    uci.do_isready(line="")
    captured = capsys.readouterr().out
    assert captured == "readyok\n"

    # empty input must not crash engine
    uci.default(line="")
    assert True


def test_debug(capsys):
    engine = Engine(protocol="uci")
    uci = UCI(engine=engine)
    uci.do_uci(line="")
    capsys.readouterr()

    uci.do_debug("on")
    captured = capsys.readouterr().out
    assert captured == "info string debug mode enabled\n"
    uci.do_debug("off")


# def test_options(self, capsys)


def test_move(capsys):
    engine = Engine(protocol="uci")
    uci = UCI(engine=engine)
    uci.do_uci(line="")
    uci.do_isready(line="")
    # grab once and send to dev/null
    capsys.readouterr()

    uci.do_ucinewgame(line="")
    uci.do_position(line="startpos")
    uci.do_go(line="go wtime 122000 btime 120000 winc 2000 binc 2000")

    engine_messages = capsys.readouterr().out  # .split('\n')
    # print(engine_messages)
    assert engine_messages.find("into depth ")
    assert engine_messages.find("into seldepth ")
    assert engine_messages.find("into nodes ")
    assert engine_messages.find("into hashfull ")
    assert engine_messages.find("bestmove ")


def test_position_initial(capsys):
    engine = Engine(protocol="uci")
    uci = UCI(engine=engine)
    uci.do_uci(line="")
    uci.do_isready(line="")
    uci.do_ucinewgame(line="")
    # grab once and send to dev/null
    capsys.readouterr()

    uci.do_position(line="startpos moves d2d4 d7d5")
    engine_messages = capsys.readouterr().out.split("\n")
    # print(engine_messages)
    assert "info string setting up initial board" in engine_messages
    assert "info string moving d2d4" in engine_messages
    assert "info string moving d7d5" in engine_messages


def test_position_fen(capsys):
    engine = Engine(protocol="uci")
    uci = UCI(engine=engine)
    uci.do_uci(line="")
    uci.do_isready(line="")
    uci.do_ucinewgame(line="")
    # grab once and send to dev/null
    capsys.readouterr()

    uci.do_position(line="fen 1k6/1p6/8/8/8/8/P7/K7 w - - 0 1 moves a2a3")
    engine_messages = capsys.readouterr().out.split("\n")
    # print(engine_messages)
    assert "info string setting up board from fen 1k6/1p6/8/8/8/8/P7/K7 w - - 0 1" in engine_messages
    assert "info string moving a2a3" in engine_messages


def test_king_castling():
    e = Engine(protocol="uci")
    e.board = Board(fen="k7/8/8/8/8/8/8/R3K2R w KQkq - 0 1")

    e.make_move(any_notation="Ke1g1", color="w")

    assert e.board.get_piece_at(position="e1") == "."
    assert e.board.get_piece_at(position="f1") == "R"
    assert e.board.get_piece_at(position="g1") == "K"
    assert e.board.get_piece_at(position="h1") == "."

    # same for black
    e.board = Board(fen="k7/8/8/8/8/8/8/R3K2R w KQkq - 0 1")
    e.board.flip()

    e.make_move(any_notation="ke8g8", color="b")

    assert e.board.get_piece_at(position="e8") == "."
    assert e.board.get_piece_at(position="f8") == "r"
    assert e.board.get_piece_at(position="g8") == "k"
    assert e.board.get_piece_at(position="h8") == "."


def test_queen_castling():
    e = Engine(protocol="uci")
    e.board = Board(fen="k7/8/8/8/8/8/8/R3K2R w KQkq - 0 1")

    e.make_move(any_notation="Ke1c1", color="w")

    assert e.board.get_piece_at(position="b1") == "."
    assert e.board.get_piece_at(position="c1") == "K"
    assert e.board.get_piece_at(position="d1") == "R"
    assert e.board.get_piece_at(position="e1") == "."

    # same for black
    e.board = Board(fen="k7/8/8/8/8/8/8/R3K2R w KQkq - 0 1")
    e.board.flip()

    e.make_move(any_notation="ke8c8", color="b")

    assert e.board.get_piece_at(position="b8") == "."
    assert e.board.get_piece_at(position="c8") == "k"
    assert e.board.get_piece_at(position="d8") == "r"
    assert e.board.get_piece_at(position="e8") == "."
