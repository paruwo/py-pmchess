from pmc.board import Board
from pmc.move import Move
from pmc.piece import Piece


def test_initial_board():
    b = Board()
    assert b.moves == []


def test_setup_from_fen():
    b = Board("rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2")
    assert b is not None


def test_get_piece_at():
    board = Board()
    assert board.get_piece_at("a1") == "R"
    assert board.get_piece_at("h1") == "R"
    assert board.get_piece_at("a8") == "r"
    assert board.get_piece_at("h8") == "r"

    assert board.get_piece_at("c4") == "." == Piece.EMPTY.value


def def_set_captured_piece():
    board = Board()
    m = Move("c4", color="w")
    board.make_move(m)
    m2 = Move("d5", color="b")
    board.make_move(m2)

    m3 = Move("c4xd", color="w")
    board.make_move(m3)
    assert m3.captured_piece == "p"


def test_unmake_move_pawn():
    board = Board()

    # must not break
    board.unmake_move()

    # move white
    m = Move("c4", color="w")
    board.make_move(m)
    assert len(board.moves) == 1
    assert board.get_piece_at("d7") == "p"
    assert board.get_piece_at("d6") == "."
    assert board.get_piece_at("d5") == "."

    assert board.get_piece_at("c4") == "P"
    assert board.get_piece_at("c3") == "."
    assert board.get_piece_at("c2") == "."

    # move black
    m2 = Move("d5", color="b")
    board.make_move(m2)
    assert len(board.moves) == 2

    assert board.get_piece_at("d7") == "."
    assert board.get_piece_at("d6") == "."
    assert board.get_piece_at("d5") == "p"

    assert board.get_piece_at("c4") == "P"
    assert board.get_piece_at("c3") == "."
    assert board.get_piece_at("c2") == "."

    # un-move black
    board.unmake_move()
    assert len(board.moves) == 1

    assert board.get_piece_at("d7") == "p"
    assert board.get_piece_at("d6") == "."
    assert board.get_piece_at("d5") == "."

    assert board.get_piece_at("c4") == "P"
    assert board.get_piece_at("c3") == "."
    assert board.get_piece_at("c2") == "."

    # un-move white
    board.unmake_move()
    assert len(board.moves) == 0

    assert board.get_piece_at("d7") == "p"
    assert board.get_piece_at("d6") == "."
    assert board.get_piece_at("d5") == "."

    assert board.get_piece_at("c4") == "."
    assert board.get_piece_at("c3") == "."
    assert board.get_piece_at("c2") == "P"


def test_unmake_move_queen():
    board = Board(fen="rnbqkbnr/ppppp1pp/5p2/8/8/4P3/PPPP1PPP/RNBQKBNR b KQkq - 0 1")

    # move white
    m = Move("Qd1h5", color="w")
    assert board.get_piece_at("d1") == "Q"
    assert board.get_piece_at("h5") == "."

    board.make_move(m)
    assert board.get_piece_at("h5") == "Q"
    assert board.get_piece_at("d1") == "."

    board.unmake_move()
    assert board.get_piece_at("d1") == "Q"
    assert board.get_piece_at("h5") == "."


def test_unmake_move_bishop():
    board = Board(fen="rnbqkbnr/ppppp1pp/5p2/8/8/4P3/PPPP1PPP/RNBQKBNR b KQkq - 0 1")

    # move white
    m = Move("Bf1a6", color="w")
    assert board.get_piece_at("f1") == "B"
    assert board.get_piece_at("a6") == "."

    board.make_move(m)
    assert board.get_piece_at("f1") == "."
    assert board.get_piece_at("a6") == "B"

    board.unmake_move()
    assert board.get_piece_at("f1") == "B"
    assert board.get_piece_at("a6") == "."


def test_flip():
    # trivial example
    initial_board = Board()
    board = Board()

    # two times flip must be original again
    board.flip()
    board.flip()

    assert board.str_board == initial_board.str_board

    # non-trivial example taken from https://www.chessprogramming.org/Color_Flipping
    board = Board(fen="r1bqk1nr/pppp1ppp/2n5/2b1p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w KQkq -")
    flipped_board = Board(fen="rnbqk2r/pppp1ppp/5n2/2b1p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR b KQkq -")

    board.flip()
    assert board.fen == flipped_board.fen


def test_find_piece():
    board = Board()
    assert board.find_piece(piece="K") == ["e1"]
    assert board.find_piece(piece="k") == ["e8"]
    assert board.find_piece(piece="R") == ["a1", "h1"]
    assert board.find_piece(piece="q") == ["d8"]


def test_last_moves():
    board = Board()
    # 1
    board.make_move(Move(san="a4", color="w"))
    board.make_move(Move(san="a6", color="b"))

    # 2
    board.make_move(Move(san="b3", color="w"))
    assert board.last_moves_str() == "1. a4 a6  2. b3"
    board.make_move(Move(san="b5", color="b"))

    # 3
    board.make_move(Move(san="c4", color="w"))
    board.make_move(Move(san="c6", color="b"))
    assert board.last_moves_str() == "1. a4 a6  2. b3 b5  3. c4 c6"

    # 4
    board.make_move(Move(san="d4", color="w"))
    assert board.last_moves_str() == "2. b3 b5  3. c4 c6  4. d4"
    board.make_move(Move(san="d6", color="b"))
    assert board.last_moves_str() == "2. b3 b5  3. c4 c6  4. d4 d6"


def test_get_fen():
    board = Board()

    assert board.get_fen() == "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"  # w KQkq - 0 1'


def test_str():
    board = Board()
    assert (
        str(board) == "    --------\n"
        "8 | rnbqkbnr   0. move, w to move\n"
        "7 | pppppppp\n"
        "6 | ........   hash=15565827975998637704\n"
        "5 | ........\n"
        "4 | ........\n"
        "3 | ........\n"
        "2 | PPPPPPPP   fen is rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR\n"
        "1 | RNBQKBNR\n"
        "    ________\n"
        "    ABCDEFGH\n"
    )
