from pmc.board import Board
from pmc.move_generator import KnightMoveGenerator


def test_moves():
    board = Board()
    moves = KnightMoveGenerator.generate(board=board, position="b1", color="w")

    assert len(moves) == 2
    assert "Nb1a3" in [m.san for m in moves]
    assert "Nb1c3" in [m.san for m in moves]

    # same but for black
    board = Board()
    board.flip()
    moves = KnightMoveGenerator.generate(board=board, position="b8", color="b")

    assert len(moves) == 2
    assert "nb8a6" in [m.san for m in moves]
    assert "nb8c6" in [m.san for m in moves]


def test_moves_capture():
    board = Board(fen="8/K7/8/8/3N4/5p2/8/7k w - - 0 1")
    moves = KnightMoveGenerator.generate(board=board, position="d4", color="w")

    assert "Nd4xf3" in [m.san for m in moves]
    assert len(moves) == 8

    # same but for black
    board = Board(fen="8/K7/8/8/3N4/5p2/8/7k w - - 0 1")
    board.flip()
    moves = KnightMoveGenerator.generate(board=board, position="d5", color="b")

    assert "nd5xf6" in [m.san for m in moves]
    assert len(moves) == 8
