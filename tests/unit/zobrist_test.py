from pmc.move import Move
from pmc.zobrist import ZobristHash


def test_hash_full():
    zh = ZobristHash()
    assert zh.hash_value == 15565827975998637704


def test_hash_incremental_pawn():
    """Simply move pawn and un-move."""
    zh = ZobristHash()
    assert zh.hash_value == 15565827975998637704

    m = Move("c4", color="w")
    zh.incremental_hash(move=m)
    assert zh.hash_value == 9279936907258406217

    # unmake move means to apply the same move once again
    m = Move("c4", color="w")
    zh.incremental_hash(move=m)
    assert zh.hash_value == 15565827975998637704

    # same in black
    m = Move("c7", color="b")
    zh.incremental_hash(move=m)
    assert zh.hash_value == 16219683483585616818

    # unmake move means to apply the same move once again
    m = Move("c7", color="b")
    zh.incremental_hash(move=m)
    assert zh.hash_value == 15565827975998637704
