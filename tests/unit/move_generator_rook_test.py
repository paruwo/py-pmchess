from pmc.board import Board
from pmc.move_generator import RookMoveGenerator


def test_sliding_moves():
    # simple to right, no move
    board = Board(fen="R7/K7/8/8/8/4p3/8/7k w - - 0 1")
    moves = RookMoveGenerator.generate(board=board, position="a8", color="w")
    assert len(moves) == 7

    # same but for black
    board = Board(fen="R7/K7/8/8/8/4p3/8/7k w - - 0 1")
    board.flip()
    moves = RookMoveGenerator.generate(board=board, position="a1", color="b")
    assert len(moves) == 7


def test_simple_left():
    board = Board(fen="7R/7K/8/8/8/4p3/8/7k w - - 0 1")
    moves = RookMoveGenerator.generate(board=board, position="h8", color="w")
    assert len(moves) == 7

    # same but with black
    board = Board(fen="7R/7K/8/8/8/4p3/8/7k w - - 0 1")
    board.flip()
    moves = RookMoveGenerator.generate(board=board, position="h1", color="b")
    assert len(moves) == 7


def test_left_right_down_with_enemy():
    board = Board(fen="1r2R3/K7/8/8/8/4p3/8/7k w - - 0 1")
    moves = RookMoveGenerator.generate(board=board, position="e8", color="w")
    assert len(moves) == 11
    assert "Re8xb8" in [m.san for m in moves]
    assert "Re8xe3" in [m.san for m in moves]

    # same but with black
    board = Board(fen="1r2R3/K7/8/8/8/4p3/8/7k w - - 0 1")
    board.flip()
    moves = RookMoveGenerator.generate(board=board, position="e1", color="b")
    assert len(moves) == 11
    assert "re1xb1" in [m.san for m in moves]
    assert "re1xe6" in [m.san for m in moves]


def test_all_directions_with_one_enemy():
    board = Board(fen="8/K3p3/8/1r2R2n/8/4p3/8/7k w - - 0 1")
    moves = RookMoveGenerator.generate(board=board, position="e5", color="w")
    assert len(moves) == 10
    assert "Re5xe7" in [m.san for m in moves]
    assert "Re5xe3" in [m.san for m in moves]
    assert "Re5xb5" in [m.san for m in moves]
    assert "Re5xh5" in [m.san for m in moves]

    # same but with black
    board = Board(fen="8/K3p3/8/1r2R2n/8/4p3/8/7k w - - 0 1")
    board.flip()
    moves = RookMoveGenerator.generate(board=board, position="e4", color="b")
    assert len(moves) == 10
    assert "re4xe2" in [m.san for m in moves]
    assert "re4xe6" in [m.san for m in moves]
    assert "re4xb4" in [m.san for m in moves]
    assert "re4xh4" in [m.san for m in moves]
