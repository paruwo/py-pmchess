import logging

from pmc.board import Board
from pmc.move_generator import KingMoveGenerator, MoveGenerator


def test_moves():
    board = Board(fen="8/2PKp3/8/1r2R2n/8/4p3/8/7k w - - 0 1")

    logging.getLogger().setLevel(logging.DEBUG)
    moves = KingMoveGenerator.generate(board=board, position="d7", color="w")
    logging.getLogger().setLevel(logging.INFO)

    assert len(moves) == 6
    assert "Kd7c8" in [m.san for m in moves]
    assert "Kd7d8" in [m.san for m in moves]
    assert "Kd7e8" in [m.san for m in moves]

    assert "Kd7c6" in [m.san for m in moves]
    assert "Kd7d6" not in [m.san for m in moves]  # black pawn attacks
    assert "Kd7e6" in [m.san for m in moves]

    assert "Kd7xe7" in [m.san for m in moves]

    # same but for black
    board = Board(fen="8/2PKp3/8/1r2R2n/8/4p3/8/7k w - - 0 1")
    board.flip()
    moves = KingMoveGenerator.generate(board=board, position="d2", color="b")

    assert len(moves) == 6
    assert "kd2c1" in [m.san for m in moves]
    assert "kd2d1" in [m.san for m in moves]
    assert "kd2e1" in [m.san for m in moves]
    assert "kd2c3" in [m.san for m in moves]
    assert "kd2d3" not in [m.san for m in moves]  # white pawn attacks
    assert "kd2e3" in [m.san for m in moves]
    assert "kd2xe2" in [m.san for m in moves]


def test_moves_corner():
    board = Board(fen="7k/8/8/8/8/8/8/7K w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="h1", color="w")
    assert len(moves) == 3

    # same but black
    board = Board(fen="7k/8/8/8/8/8/8/7K w - - 0 1")
    board.flip()
    moves = KingMoveGenerator.generate(board=board, position="h8", color="b")
    assert len(moves) == 3


def test_can_king_castle_all():
    board = Board(fen="5k2/8/8/8/8/8/8/R3K2R w KQ -")
    moves = KingMoveGenerator.generate(board=board, position="e1", color="w")

    assert board.castling == "KQ"
    assert "Ke1c1" in [m.san for m in moves]
    assert "Ke1g1" in [m.san for m in moves]
    assert True in [m.is_kingside_castling for m in moves]
    assert True in [m.is_queenside_castling for m in moves]
    # assert num_castlings == 2

    # same for black
    board = Board(fen="5k2/8/8/8/8/8/8/R3K2R w KQ -")
    board.flip()
    moves = KingMoveGenerator.generate(board=board, position="e8", color="b")

    assert board.castling == "kq"
    assert "ke8c8" in [m.san for m in moves]
    assert "ke8g8" in [m.san for m in moves]
    assert True in [m.is_kingside_castling for m in moves]
    assert True in [m.is_queenside_castling for m in moves]
    # assert num_castlings == 2


def test_can_king_castle_king_side():
    board = Board(fen="5k2/8/8/8/8/8/8/R3K2R w K -")
    moves = KingMoveGenerator.generate(board=board, position="e1", color="w")

    assert board.castling == "K"
    assert "Ke1g1" in [m.san for m in moves]
    assert True in [m.is_kingside_castling for m in moves]
    assert True not in [m.is_queenside_castling for m in moves]
    # assert num_castlings == 1

    # same in black
    board = Board(fen="5k2/8/8/8/8/8/8/R3K2R w K -")
    board.flip()
    moves = KingMoveGenerator.generate(board=board, position="e8", color="b")

    assert board.castling == "k"
    assert "ke8g8" in [m.san for m in moves]
    assert True in [m.is_kingside_castling for m in moves]
    assert True not in [m.is_queenside_castling for m in moves]
    # assert num_castlings == 1


def test_can_king_castle_queen_side():
    board = Board(fen="5k2/8/8/8/8/8/8/R3K2R w Q -")
    moves = KingMoveGenerator.generate(board=board, position="e1", color="w")

    assert board.castling == "Q"
    assert "Ke1c1" in [m.san for m in moves]
    assert True in [m.is_queenside_castling for m in moves]
    assert True not in [m.is_kingside_castling for m in moves]
    assert sum([1 for m in moves if m.is_kingside_castling or m.is_queenside_castling]) == 1


def test_king_under_attack_by_rook_move_away():
    board = Board(fen="3k4/8/K6r/P7/8/8/8/8 w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="a6", color="w")

    assert len(moves) == 3
    # move away out of check
    assert "Ka6a7" in [m.san for m in moves]
    assert "Ka6b5" in [m.san for m in moves]
    assert "Ka6b7" in [m.san for m in moves]
    # keep in check is prohibited and therefore an invalid move
    assert "Ka6b6" not in [m.san for m in moves]  # rook


def test_king_under_attack_by_bishop_and_rook_move_away():
    board = Board(fen="3k4/8/K6r/P2b4/8/8/8/8 w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="a6", color="w")

    assert len(moves) == 2
    # move away out of check
    assert "Ka6a7" in [m.san for m in moves]
    assert "Ka6b5" in [m.san for m in moves]
    # keep in check is prohibited and therefore an invalid move
    assert "Ka6b6" not in [m.san for m in moves]  # rook
    assert "Ka6b7" not in [m.san for m in moves]  # bishop


def test_king_under_attack_by_bishop_and_knight_move_away():
    board = Board(fen="3k4/8/K2n4/P2b4/8/8/8/8 w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="a6", color="w")

    assert len(moves) == 2
    # move away out of check
    assert "Ka6a7" in [m.san for m in moves]
    assert "Ka6b6" in [m.san for m in moves]
    # keep in check is prohibited and therefore an invalid move
    assert "Ka6b5" not in [m.san for m in moves]  # knight
    assert "Ka6b7" not in [m.san for m in moves]  # bishop


def test_king_under_attack_by_rook_and_king_move_away():
    board = Board(fen="8/2k5/K6r/P7/8/8/8/8 w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="a6", color="w")

    assert len(moves) == 2
    # move away out of check
    assert "Ka6a7" in [m.san for m in moves]
    assert "Ka6b5" in [m.san for m in moves]
    # keep in check is prohibited and therefore an invalid move
    assert "Ka6b6" not in [m.san for m in moves]  # rook
    assert "Ka6b7" not in [m.san for m in moves]  # king


def test_king_under_attack_by_knight_and_pawn_move_away():
    board = Board(fen="3k4/2p5/K1n5/P7/8/8/8/8 w - - 0 1")
    moves = KingMoveGenerator.generate(board=board, position="a6", color="w")

    assert len(moves) == 2
    # move away out of check
    assert "Ka6b7" in [m.san for m in moves]
    assert "Ka6b5" in [m.san for m in moves]
    # keep in check is prohibited and therefore an invalid move
    assert "Ka6a7" not in [m.san for m in moves]  # knight
    assert "Ka6b6" not in [m.san for m in moves]  # pawn


def test_king_in_check_move_away_dont_capture():
    board = Board(fen="k7/r7/8/8/8/8/8/K6R w - - 0 1")
    mg = MoveGenerator(setup_board=board)
    mg.generate()
    assert mg.is_king_under_attack
    final_move = mg.best_move()

    assert final_move.piece == "K"


def test_king_in_check_capture_attacking_piece():
    board = Board(fen="k7/r6R/1r6/8/8/8/8/K7 w - - 0 1")
    pass


def test_king_in_check_block_ray_attack():
    board = Board(fen="k7/r7/1r6/8/8/8/8/K1B5 w - - 0 1")
    pass
