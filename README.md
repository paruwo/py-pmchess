# PMC

![logo](resource/logo.jpg)

(Py)PmChess, or shorter PMC, is the Poor Man's Chess game engine.
It was born from the idea of recreational programming 😎 combined with practising and improving programming skills.

The very first version was started in Rust and later code was changed to C++. Sadly, with the insight that
learning modern C++ is not very recreational to me as well as the fact that Python is the language of choice
in my real-world job, I moved to Python. So obviously, this might never become a fast top-100 chess engine by any means.

... except if parts of core are ported to a compiled environment. 🤓

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## 🚏 Not yet ready

This is still in an early stage and cannot play chess 100% correctly.
Of course, feel free to take a tea and take a look around.

### v0.1 (currently in progress)

* ✓ create in 🐍 Python 3.11+ from scratch
  * code: board, move, naive move generator
  * tests and test coverage facility
  * first ideas for performance test suite
* ✓ 🖧 UCI protocol (tested with CuteChess and PyChess)
* ✓ 📖 documentation
  * how to play with PyChess
  * how to build and test
* ✓ add Docker container
* ✓ ⏱ prepare for performance measurement
  * bootstrap some other engines
  * bootstrap one rating engine
  * board hashing
  * basic transposition table
* ⏳♚ chess
  * ✓ basic move generation for all pieces
  * ✓ don't move king into check
  * zugzwang / king safety (TODO block ray attack, capture attacker, give up; DONE move away)
  * complete 99/100 games without technical error using UCI (CuteChess Tournament)

### v0.2 (next up)

* ♚ chess
  * depth-first search based on game tree
  * simple move ordering and basic strategies
  * win &gt;80% again random mover
  * chess test suites &gt;10% successful (best move, avoid move)
  * 50 move rule
  * piece pinning
* 🖧 protocols
  * finalize CECP protocol (add scripts for winboard/xboard)
  * unit test CECP
* 🖥 tech
  * use transposition table and measure performance gains
  * implement automated lightweight pipeline for unit and performance tests
  * implement scripted cpu vs. cpu matches
  * implement basic board evaluation


## Table of contents

1. [Basic Principles](#principles)
2. [Features](#features)
3. [Prerequisites](#prerequisites)
4. [Build from Source](#source)
5. [Used Software](#used_software)
6. [References](#references)
7. [Log](#log)
8. [TODO](#todo)

### Basic principles {#principles}

* clean, readable and portable code
* statically analyzed code
* unit tests with high coverage
* continuous integration
* liberal license and use of liberally licensed tools
* no alien tools or libraries

### Features {#features}

* 🙾 board
  * mailbox board
* 🔎 search
  * naive brute-force move generation with search depth 1
* 🧠 evaluation
  * nothing yet

Planned features are

* board: precomputed move tables, 64bit bitboards for move generation
* search: αβ pruning
* eval: heuristic function
* eval: machine learning?

### Run & Test

You can play against this engine but still with limitations only. The engine has not yet all chess rules implemented.
The usage of any chess GUI is highly recommended. You can find instructions for adding the pmc engine below.

Currently, you have two options to run it: run in a Python venv or build and use a docker container.

#### Setup Python environment

Install pyenv and poetry. If pyenv and plugins are not installed and pyenv errors pop up, then prefix all calls with "pyenv exec ".

```shell
# install dependencies and mydarts, once only
pyenv virtualenv 3.12 py-pmchess
pyenv local py-pmchess
# .. terminal switches to venv OR
# pyenv activate py-pmchess OR
# run with "pyenv exec ..."
pip3 install --upgrade pip
pip3 install poetry==1.8.4
poetry install
```


For a development setup, just install pre-commit hooks and on-demand
use one or more of these tools:

```shell
# run the unit tests
pytest tests/unit

# run the perft unit tests (correctnesss of move generator)
pytest tests/perft
# python3 -m cProfile -s tottime pmc/perft.py

# run all performance tests
pytest \
  --benchmark-autosave \
  --benchmark-compare \
  --benchmark-disable-gc \
  --benchmark-name=short \
  --benchmark-sort=name \
  tests/performance


# check for security issues
trivy fs . --scanners vuln
trivy config .

# bootstrap competitive engine and tournament software
cd bin
python bootstrap.py
```


#### Run dockerized

Here be dragons.
To play using a docker image, you only need to run the build command once and then use one of the provided
scripts in the bin/&lt;your os&gt; folder.

```shell
# build once
docker build -t paruwo/pmc .

# for interactive debugging use this (mind switch "-t")
docker run -it --rm --env PROTOCOL=--uci --name pmc-app paruwo/pmc
```

### Use it with GUI

Working frontends are BanksiaGUI, Cutechess and Winboard.

### qperft as a performance reference

To build qperf as performance reference switch "dev" folder and compile it.
Ensure that bootstrapping was done before.

Sadly, the c program has some issues.
You need to at least add "#include&lt;string&gt;" to the header to let it compile.
The management of hashtable size is also buggy, but we don't need it here.

```shell
# *nix
cd resource/engines
gcc -ansi -Ofast -mtune=native -march=native -lm perft.c -o perft

# Windows
cl /O2 /arch qperf.c

# run perft 6 without hashtable
./qperft 6
```

### Log {#log}

* Q1 2025

Pick from attic and refresh setup and libs

* Q2 2021

Implement basic move generator, perft and divide debugging functionality, add basic performance tests,
many unit tests, dockerized, Zobrist board hashing

* Q1 2021

Switched to Python to gain real personal benefit, most rules done but without focus on performance.

Switched build tool from CMake to meson, move from cxxtest to gtest, remove very old compiler macro handling regarding
modern CPU features, replace library possit (no more existing) by compiler macros

* Q2 2020

modernization and clean up, prepare for fixing real bugs

* HY1 2018

update plog and tclap and omit their warnings, include cppcheck and cxxtext and doxygen into waf properly, test lizard
code metric tool, replace plog and tclap by boost, add strategic test suite STS, switch from waf to CMake, splitted code
and tests into subprojects, introduce trace log level

* HY2 2017

finish SAN representation, split protocol library, remove dependency to waftools by deliverying the required files,
tried hardening flags

* Q2 2017

Added parameter for number of worker threads, added benchmark document, basic communication of CECP works also being
prepared for future feature additions, enhance EPD parser, add EPD test suite fuctionality and add execution parameter
for EPD testsuite execution, add unit tests for higher coverage, think about opening book implementation, first win
against random moving POS

* Q1 2017

Replaced spdlog by plog, fixed debug switch in build script, fixed fallback for nth bit implementation, read chess
papers, code refactorings, more unit test, separate and rebuild move generator, fixed wafscripts for python3, perft
result class, more tests and higher coverage, implement parallel perft-divide

* November/December 2016

added new Doxygen documentation configuration and began documentation, started to refactor BBUtil, did not find
header-only cuckoo hash table lib so used simple vector for transposition table, replaced easylogging by spdlog

* September/October 2016

disable bit twiddling optimizations, add table of used software, added BMI2 runtime check, fixed UTs, completes UCI
protocol, began WinBoard protocol, optimized move encoding, implemented random move, switch von local svn to cloud git
repo, added non-Unicode pieces, first soft threading support, cleaned up duplicate declarations, implemented simple
perft, added capturing piece to move, replaced vector by deque internally, added manpage

* July/August 2016

apply Apache 2.0 license, grab licenses for referenced software, applied C++ best practices, fixed many sign errors and
implicite casts, added "#pragma GCC system_header" to all external libraries in order not to get their warnings, test
further C++ IDEs, removed OpenMP lib from path, made OS-independent, added Windows as supported platform, trying out
further unit test libs, added costexpr where possible, small UCI fixes, used tiny library
"possit" to get correct CPUID flags, added more compilation feature output for interactive mode

* June 2016

some first localization tests using boost_locale and poedit, add tclap fix to includes, fix boolean/choice build
parameters, document gcov/lcov and cppcheck usage, fix test coverage usage and report, extend unit tests, add compiler
version checks, consolidate cppcheck and doxygen and gcov under reports/, build test in cygwin, remove boost_locale
again to enable build in Windows

* May 2016

added program parameters with local tclap, improve move encoding of data, fixed all cppcheck findings, added endgame
Gaviota tablebase for 4 pieces to /resource, restructure into main library libpmchess3 and program pmchess that is
linked against this library, added conditional configuration for includes, configured source delivery (distcheck works),
begin UCI implementation tested with PyChess, UCI registration, installed JOSE in /usr/share/games, reintroduced a build
system - WAF, made unit tests + doxygen + test execution to work in WAF, included Gaviota tablebase c++ libs including
tiny test, removed platforms to concentrate on further features

* April 2016

create intermediate objects to separate compiling and linking to allow proper unit testing, introduced unit test
framework cxxtest, renice README, externalized Bitboard Utils for unit testing, enabled C++ linter, fixed some linter
messages, reformatted code, added UTs for BBUtil and Bitboard, fixed bug to include headers only once, flipping and
rotating board, testing Kogge-Stone

* March 2016

completed FEN setup, switched back to easylogging++, CTZ optimization, switched to GCC (better intrisics support),
Kogge-Stone ray attacks, started FEN setup, add Cygwin switch in build.sh, test knightfill, implement Move, implement
move source and target position, wrap move generator for pawn and knight and king, nth_bit function and tests (used or
pawns), replace #define by functions, begin basic bitboard implementation, board representation as list of bitboards (
set based approach), printing, setters, some first bit magic, king attack lookup table, knight attack, clz and ctz,
single and double pawn pushes, rest of move lookup tables

### OLD TODO {#todo}

Some general stuff to do:

* [Check Rebel](http://rebel13.nl/rebel13/rebel%2013.html)
* extend tests for UCI protocol
* xboard protocol [1](http://home.hccnet.nl/h.g.muller/interfacing.txt), [2](http://home.hccnet.nl/h.g.muller/engine-intf.html#9)
* way to better chess engine
  * according to literature chess knowledge, proper purging, move ordering etc. are much more important than fast move
      generation. that's why after having made pmc somewhat robust and error-free (point of start) we start with making
      it smarter, then concentrate on making it faster, then smater again etc. So we do a kind of tick-tock.
* point of start - "salty" pre-release 0.1
  * &#x2713; make code modular and extensible
  * &#x2713; implement basic perft tests with do/undo
  * &#x2713; implement threaded debugging tool "divide"
  * &#x2713; &#x2265;5M NPS perft on one test board (in release mode)
  * &#x2713; implement UCI or CECP protocols to work basically
    * &#x2713; first win against random mover
    * &#x2713; create or select royalty-free engine icon (death to the king)
    * setup chess test like [this](https://www.schach-computer.info/wiki/index.php?title=Kategorie:Stellungstests) or Bratko-Kopec
      Test etc.)
    * make move generator to generate fully-valid moves
    * setup test environment (vagrant and ansible scripts)
    * setup test tools (elobayes, ordo, cutechess, random mover, doxygen, ...
    * complete &gt;99% games without errors, &gt;90% wins again random mover
    * prepare system to enable full feature toggle
    * create an ordo test arena with first rough strength estimation
    * finalize man page
    * create first public binary and source snapshots
* smarter - "grip" 1.0
  * debug generator for critical positions
  * implement dead-simple game tree
  * first chess tactics knowledge
    * opening: develop officers, capture center, prefer rochade
    * midgame: in case of material benefit perfer tausch
    * endgame: push opposite king into corner, save own king
    * distiguish between human and computer opponent
    * consider material, mobility
  * recognition of the 3 game phases and adaption of tactics accordingly
  * mini-max or better (eventually a/b pruning algorithm)
  * expose internal chess engine parameters into config file (formula parameters)
  * setup central CI environment
  * [local test with LCT II](https://chessprogramming.wikispaces.com/LCT+II)
  * set of classical chess endings
  * add pondering / permament brain capabilities
  * deal with given time limits
  * move ordering and selection for every of the three game phases
  * use opening book, evaluate and test [this](http://hgm.nubati.net/book_format.html)
  * use endgame book (implement, Gaviota already linkable)
  * [improved setup tools](http://www.arasanchess.org/tests.shtml)
  * more perft tests to prove correctness
  * finalize UCI / CECP protocol implementation
  * informational output about engine state
  * should win &gt;99% against random and 10% against another weak engine
  * remove code duplications, clean up classes, better logging
  * all perft tests work properly and no game is ended because of invalid move in a long period of time
* faster - "roadrunner" 1.5
  * code rework
    * efficiency of encoding
    * improve caching and exploite cache lines
    * apply C++ optimization guidelines (move constructors etc.)
    * [check this](http://www.tantalon.com/pete/cppopt/main.htm)
  * implement better pruning (node sorting etc.)
  * add global Zobrist hash table and use hashing
  * reimplement tree and use scalable structures
  * &gt;50M NPS perft (x10, stockfish divide performance)
  * more perft tests, reach perft(7) in under 1 minute
  * collect internal KPIs
  * informational output about performance
  * do measure all activities and features (see stockfishtest)
  * compete with top-450 engines (1500 ELO)
* smarter - "fox" 2.0
  * add engine throttling
  * add evaluation mode
  * more chess knowledge / tactics
    * double attack
    * open files for rooks
    * pawns securing king
    * ...
  * better heuristics and pruning
    * killer
    * late
    * null
  * tune parameters (formulas, piece value, ...)
  * leverage all top test suites
  * compete with top-325 engines (2000 ELO)
* faster - "geisterfaust" 2.5
  * more efficient and parallel bulk move generation
  * non-blocking parallel tree evaluation (hard)
  * increase efficiency of existing hash table
  * add pieces hash tables (pawns etc.)
  * &gt;200M NPS perft (x4, same as Muller's qperf)
* smarter - "atahualpa" 3.0
  * switch all algorithms to those that scale well in concurrent systems (hard)
  * compete with top 150 engines (2500 ELO)
* faster - "thule" 3.5
  * let this be the last brute-force evolution step
  * try out OpenCL (Boost.Compute)
  * try out cuckoo hash tables
  * try to achieve &gt;400M NPS in perft (x2)
  * compete with top 100 engines (2650 ELO)
  * (Is this the end of Shannon approach? Or go further with magic bitboards, ... )
* smarter - "sol" 4.0
  * refresh simple machine learning, search for current applications (POS)
  * think about deep learning libraries like [tiny-dnn](https://github.com/tiny-dnn/tiny-dnn)
* ... what's next?

More general features to add:

* use libraries
  * opening book (BIN - Polyglot, HIARCS, CTG - ChessBase, ABK - Arena)
* search and evaluation
  * transposition table
  * pawn tables
  * principal variation search (PVS)
  * iterative deepening
  * move ordering
    * MVV-LVA (Most Valuable Victim - Least Valuable Aggressor)
  * selectivity
    * alpha-beta pruning
      * dynamic move ordering
      * killer heuristic
    * late move reductions
    * null move pruning
* efficiency
  * pondering
* throttling
  * strength limitation down to random mode

PyChess configuration at ~/.config/pychess/engines.json (and ~/.local/share/pychess/)

XBoard configuration at ~/.xboardrc

Royalty-free logos taken from [here](https://pixabay.com/en/chess-chess-board-pawn-game-wooden-1251254/) and [there](https://pixabay.com/en/chess-king-queen-win-lose-skull-2312810/).
