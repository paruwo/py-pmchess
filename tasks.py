from pathlib import Path
from sys import platform

from invoke import task


@task
def clean(c):
    patterns = [
        Path("tests/__pycache__/"),
        Path("tests/unit/__pycache__/"),
        Path("tests/perft/__pycache__/"),
        Path("tests/.pytest_cache/"),
        Path("tests/performance/__pycache__/"),
        Path("tests/performance/.benchmarks/"),
        Path("tests/performance/.pytest_cache/"),
        Path("build/"),
        Path(".benchmarks/"),
        Path("tests/.benchmarks/"),
        Path("pmc/__pycache__/"),
        Path(".pytest_cache/"),
        Path("htmlcov/"),
        Path(".coverage"),
    ]

    if platform == "win32":
        delete_call = 'if exist ".\{}" rd /q /s ".\{}"'
        for pattern in patterns:
            c.run(delete_call.format(pattern, pattern))
    else:
        delete_call = "rm -rf {}"
        for pattern in patterns:
            c.run(delete_call.format(pattern))
