FROM python:3.13-slim-bookworm

ENV PROTOCOL='--uci' \
    # python
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    # poetry
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    POETRY_HOME='/usr/local'

# install poetry and clean
# # https://github.com/python-poetry/poetry
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && apt-get upgrade -y && \
    apt-get install --no-install-recommends -y && \
    apt-get install --no-install-recommends -y curl && \
    # install poetry
    curl -sSL 'https://install.python-poetry.org' | POETRY_VERSION=1.8.4 python3 - && \
    poetry --version && \
    # cleanup
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

# copy essential app project files
WORKDIR /app
COPY pmc /app/pmc
COPY pyproject.toml poetry.lock /app/
COPY resource /app/resource

# hard installations
RUN poetry install --no-interaction --no-ansi --no-dev && \
    rm -rf "$POETRY_CACHE_DIR"

# shell is needed to enable parameter replacement
ENTRYPOINT ["sh", "-c", "poetry run python3 -u -m pmc.client $PROTOCOL"]
