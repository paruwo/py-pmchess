#!/bin/sh

# for ultra fast games add "-noGUI"

xboard -fcp "./pmc-cecp.sh" -fd "." \
  -scp fairymax \
  -matchMode true \
  -debugMode true -debugfile xboard.log

# workaround for now
pkill -f "python3 -m pmc.client --cecp"
