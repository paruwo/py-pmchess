#!/bin/sh

# for ultra fast games add "-noGUI"

xboard -fcp "./pmc-cecp.sh" -fd "." \
  -scp "java -Xmx64M -jar pos.jar --io-mode xboard --brain-mode random" -sd "../../resource/engines/" \
  -matchMode true \
  -debugMode true -debugfile xboard.log

# workaround for now
pkill -f "python3 -m pmc.client --cecp"
