@echo off
cd ..
REM GUIs usually have no Python support, so we provide something executable
REM cmd /k "cd ..\venv\Scripts & activate & cd ..\..\ & venv\Scripts\python.exe -m pmc.client --cecp --debug"
cmd /k "poetry.exe run python -m pmc.client --cecp"
