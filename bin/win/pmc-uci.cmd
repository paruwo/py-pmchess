@echo off

cd %USERPROFILE%\Desktop\workstation\py-pmchess\bin\win\

REM GUIs usually have no Python support, so we provide something executable
REM cmd /k "cd ..\venv\Scripts & activate & cd ..\..\ & venv\Scripts\python.exe -m pmc.client --uci --debug"
cmd /k "poetry.exe run python -m pmc.client --uci"
