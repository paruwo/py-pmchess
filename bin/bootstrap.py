import logging
import platform
from pathlib import Path

import coloredlogs
import requests


class Bootstrapper:
    bin_folder: Path
    log: logging
    platform: str

    # another candidate could be https://sx.rosada.cz/projects/sxrandom
    generic_resources = {
        # '../resource/engines/brutus-rnd.jar': 'http://www.xs4all.nl/~vermeire/BrutusRND.rar',
        "../resource/engines/perft.c": "https://home.hccnet.nl/h.g.muller/perft.c",
        "../resource/engines/pos.jar": "https://web.archive.org/web/20201106232451/https://www.vanheusden.com/pos/pos-bin-1.20.jar",
        # '../resource/opening_books/': ''
    }

    platform_resources = {
        "Linux": {
            "../resource/rating_engines/ordo.tar.gz": "https://github.com/michiguel/Ordo/releases/download/v1.2.6/ordo-1.2.6.tar.gz",
        },
        "Darwin": {
            "../resource/rating_engines/ordo.tar.gz": "https://github.com/michiguel/Ordo/releases/download/v1.2.6/ordo-1.2.6.tar.gz",
        },
        "Windows": {
            "../resource/rating_engines/ordo.zip": "https://github.com/michiguel/Ordo/releases/download/v1.2.6/ordo-1.2.6-win.zip",
        },
    }

    def __init__(self):
        self.log = logging.getLogger()
        self.log.info("This is the py-pmc bootstrapper.")

        self.bin_folder = Path(__file__).parent
        self.platform = platform.system()

        # get generic resources
        for resource in self.generic_resources:
            self.ensure_present(resource=resource, resources=self.generic_resources)

        # get what is needed as platform-specific binary
        specific_resource = self.platform_resources[self.platform]
        for resource in specific_resource:
            self.ensure_present(resource=resource, resources=specific_resource)

    @staticmethod
    def download_file(url: str, local_filename: str):
        """This piece to download binaries was found on stackoverflow."""
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            file = Path(local_filename)
            with file.open("wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
        return local_filename

    def ensure_present(self, resource: str, resources: dict):
        """Check if expected files are present locally, else trigger download."""
        self.log.debug("ensure_present %s", resource)
        if not Path(self.bin_folder / Path(resource)).exists():
            self.log.info("downloading %s", resource)
            Bootstrapper.download_file(url=resources[resource], local_filename=resource)
        else:
            self.log.info("%s already present", resource)


if __name__ == "__main__":
    coloredlogs.install(level=logging.INFO)
    util = Bootstrapper()
