

### Divide

Used 64bit platforms

  * Legacy
    * IBM T60 notebook
    * CPU: Intel(R) Core(TM)2 CPU T7200 @ 2.00GHz
    * RAM: 2GB
    * SSD: 256GB, SATA-2
    * OS: Fedora 25 Workstation
  * Modern
    * HP ProBook 450 G5 notebook
    * CPU: Intel(R) Core(TM) i7-8550U
    * RAM: 16GB
    * SSD: 512GB, PCIe NVMe
    * OS: Debian Stable VM @ Windows 10 Pro
  * new compilation: cmake -GNinja -DCMAKE_BUILD_TYPE=Release ../ && ninja

$ ./pmchess -d &lt;depth&gt; -w &lt;worker threads&gt;

1 thread
  * d=4 - 0.07 | ...
  * d=5 - 1.67 | ...
  * d=6 - 45.6 | ...

2 threads
  * d=4 - 0.04 | ...
  * d=5 - 1.05 | ...
  * d=6 - 36.2 | ...

3 threads
  * d=4 - 0.04 | ...
  * d=5 - 0.93 | ...
  * d=6 - 32.9 | ...

4 threads
  * d=4 - 0.04 | ...
  * d=5 - 0.95 | ...
  * d=6 - 34.0 | ...
