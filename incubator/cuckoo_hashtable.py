"""
https://github.com/DeborahHier/CuckooHash/blob/master/CuckooHash.py
"""

from hashlib import sha1


class Node:
    def __init__(self, k, d):
        self.key = k
        self.data = d

    def __str__(self):
        return "(" + str(self.key) + ", " + str(self.data) + ")"


class HashTab:
    # create 2 arrays, both half the length of size
    __hash_array1: list
    __hash_array2: list
    # total number of nodes
    __num_records: int
    # total size of hash table (both lists)
    __size: int

    def __init__(self, size):
        self.__hash_array1 = [None] * (size // 2)
        self.__hash_array2 = [None] * (size // 2)
        self.__num_records = 0
        self.__size = size

    def __len__(self) -> int:
        """Return current number of keys in table."""
        return self.__num_records

    def hash_func(self, s, hash_function=sha1):
        """Execute hashing."""
        # hash twice
        # x = BitHash(s)
        # y = BitHash(s, x)
        x = hash_function(s.encode(encoding="UTF-8")).hexdigest()

        y = int(hash_function(x.encode(encoding="UTF-8")).hexdigest(), 16)
        x = int(x, 16)

        size = self.__size // 2

        return x % size, y % size

    def insert(self, k, d) -> bool:
        """
        Insert and return true, return False if the key/data is already there.
        Grow the table if necessary.
        """

        # if already there, return False (no duplicates)
        if self.find(k):
            return False

        # create a new node with key/data
        n = Node(k, d)

        # increase size of table if necessary
        if self.__num_records >= (self.__size // 2):
            self.__grow_hash()

        position1, position2 = self.hash_func(n.key)

        # start the loop checking the 1st position in table 1
        pos = position1
        table = self.__hash_array1

        #
        for _ in range(5):
            # if the position in the current table is empty
            if not table[pos]:
                # insert the node there and return True
                table[pos] = n
                self.__num_records += 1
                return True

            # else, evict item in pos and insert the item, then deal with the displaced node
            n, table[pos] = table[pos], n

            # if we're checking the 1st table right now,
            if pos == position1:
                # hash the displaced node and check its 2nd position in the 2nd table (next time through loop)
                position1, position2 = self.hash_func(n.key)
                pos = position2
                table = self.__hash_array2
            else:
                # otherwise, hash the displaced node, and check the 1st table position
                position1, position2 = self.hash_func(n.key)
                pos = position1
                table = self.__hash_array1

        # grow and rehash if we make it here
        self.__grow_hash()
        self.rehash(self.__size)
        # deal with evicted item
        self.insert(n.key, n.data)

        return True

    def __str__(self):
        """Return string representation of both tables."""
        str1 = "Table 1: [ " + str(self.__hash_array1[0])
        str2 = " Table 2: [ " + str(self.__hash_array2[0])
        for i in range(1, self.__size):
            str1 += ", " + str(self.__hash_array1[i])
        str1 += "]"

        for i in range(1, self.__size):
            str2 += ", " + str(self.__hash_array2[i])
        str2 += "]"

        return str1 + str2

    def rehash(self, size):
        """Get new hash functions and reinsert everything."""
        # get new hash functions
        # FIXME
        # https://en.wikipedia.org/wiki/Cuckoo_hashing
        # ResetBitHash()

        # create new hash tables
        temp = HashTab(size)

        # re-hash each item and insert it into the correct position in the new tables
        for i in range(self.__size // 2):
            x = self.__hash_array1[i]
            y = self.__hash_array2[i]
            if x:
                temp.insert(x.key, x.data)
            if y:
                temp.insert(y.key, y.data)

        # save new tables
        self.__hash_array1 = temp.__hash_array1
        self.__hash_array2 = temp.__hash_array2
        self.__num_records = temp.__num_records
        self.__size = temp.__size

    def __grow_hash(self):
        """Increase the hash table's size x 2."""
        new_size = self.__size * 2
        # re-hash each item and insert it into the
        # correct position in the new table
        self.rehash(new_size)

    def find(self, k):
        """Return data if there, otherwise return None."""
        # check both positions the key/data
        pos1, pos2 = self.hash_func(k)

        # could be in. return data if found
        x = self.__hash_array1[pos1]
        y = self.__hash_array2[pos2]
        if x and x.key == k:
            return x.data
        if y and y.key == k:
            return y.data

        # key can't be found
        return None

    def delete(self, k):
        """Delete the node associated with that key and return True on success."""
        pos1, pos2 = self.hash_func(k)

        x = self.__hash_array1[pos1]
        y = self.__hash_array2[pos2]
        if x and x.key == k:
            self.__hash_array1[pos1] = None
        elif y and y.key == k:
            self.__hash_array2[pos2] = None
        else:
            # the key wasn't found in either possible position
            return False
        self.__num_records -= 1

        return True


def test():
    size = 1000
    missing = 0
    found = 0

    # create a hash table with an initially small number of buckets
    c = HashTab(100)

    # Now insert size key/data pairs, where the key is a string consisting
    # of the concatenation of "foobarbaz" and i, and the data is i
    inserted = 0
    for i in range(size):
        if c.insert(str(i) + "foobarbaz", i):
            inserted += 1
    # print("There were", inserted, "nodes successfully inserted")

    # Make sure that all key data pairs that we inserted can be found in the
    # hash table. This ensures that resizing the number of buckets didn't
    # cause some key/data pairs to be lost.
    for i in range(size):
        ans = c.find(str(i) + "foobarbaz")
        if not ans or ans != i:
            print(i, "Couldn't find key", str(i) + "foobarbaz")
            missing += 1

    # print("There were", missing, "records missing from Cuckoo")

    # Makes sure that all key data pairs were successfully deleted
    for i in range(size):
        c.delete(str(i) + "foobarbaz")

    for i in range(size):
        ans = c.find(str(i) + "foobarbaz")
        if ans or ans == i:
            print(i, "Couldn't delete key", str(i) + "foobarbaz")
            found += 1
    # print("There were", found, "records not deleted from Cuckoo")


def __main():
    test()


if __name__ == "__main__":
    __main()
