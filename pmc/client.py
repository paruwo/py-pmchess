"""Frontend module to support human or machine interaction."""

import argparse
import logging
import re
import sys
from cmd import Cmd
from logging import Logger

import coloredlogs

from pmc.board import Board
from pmc.config import PROGRAM_NAME, VERSION
from pmc.move import Move
from pmc.move_generator import MoveGenerator, chess_distance

# FEN is checked not too accurately.
# Examples are the following, whereas "position" is omitted by cmd Client:
#    "position fen rnbqkbnr/pppppppp/8/8/8/5N2/PPPPPPPP/RNBQKB1R b KQkq - 1 1 moves a7a6"
#    "position startpos moves e2e4cd w"
# We also allow moves in SAN notation instead of pure coordinate notation.
from pmc.transposition_table import TranspositionTable

uci_positions_re: re = re.compile(
    r"^((fen \S+ [bw] [KQ-]+ [a-h36-]+ \d+ \d+)|(startpos))"
    r"( moves(( [a-hrnqk1-8]{2,5})+))?( [wb])?$",
    flags=re.IGNORECASE,
)


class Engine:
    """
    Interaction with the real chess engine.

    The idea is that the real chess engine itself does not need to track all moves. Instead
    a single instance of Board does this job. This is also backed by at least the UCI protocol
    which sends the setup in its "position" commands.
    """

    debug: bool
    move_generator: MoveGenerator
    board: Board
    transposition_table: TranspositionTable

    def __init__(self, protocol: str):
        """Initialize engine."""
        self.debug = True
        self.board = Board()
        self.protocol = protocol
        self.transposition_table = TranspositionTable(capacity_size=100)

        self.move_generator = MoveGenerator(setup_board=self.board, protocol=self.protocol)
        self.move_generator.debug = self.debug
        # self.num_cpus = psutil.cpu_count()

    def send_info(self, message: str):
        """Send information to protocol."""
        # "info depth 2 score cp 214 time 1242 nodes 2124 nps 34928 pv e2e4 e7e5 g1f3"
        # differentiation of what simple output and what to evaluate by GUI is up to sender
        if self.debug:
            if self.protocol == "uci":
                print("info", message)
            else:
                print("#", message)

    def setup_board(self, fen: str = None):
        """Set up the chess board initially or from given FEN string."""
        if fen:
            self.send_info("string setting up board from fen " + fen)
            self.board = Board(fen=fen)
        else:
            self.send_info("string setting up initial board")
            self.board = Board()

        self.move_generator = MoveGenerator(setup_board=self.board, protocol=self.protocol)
        self.move_generator.debug = self.debug

    def react(self, line: str = None):
        """Let the engine react on current setup with best known next move."""
        # for i in [1, 2, 3]:
        #    psutil.cpu_percent(interval=None, percpu=False)
        #    self.send_info(message='string Engine says hello ' + str(i))
        #    sleep(0.5)
        #    self.send_info(message='cpuload ' + str(psutil.cpu_percent(interval=None, percpu=False) * 10))
        #    # self.send_info(message=)

        if not self.move_generator:
            Exception("move generator not initialized")

        # self.send_info(message=f'string best move to get from cache')
        # best_move = self.transposition_table.get(hash_value=self.board.hasher.hash_value)
        # self.send_info(message=f'string got best move from cache {best_move}')
        # if not best_move:
        self.move_generator.generate(protocol=self.protocol)
        best_move = self.move_generator.best_move()
        self.board.make_move(best_move)

        self.transposition_table.put(hash_value=self.board.hasher.hash_value, best_move=best_move.san)
        self.send_info(message=f"hashfull {self.transposition_table.get_fill_state()}")

        if self.protocol == "uci":
            # moves must not contain the piece
            self.send_info(message="string generated " + str(len(self.move_generator.moves)) + " moves, best=" + best_move.to_pure())
            print("bestmove", best_move.to_pure())
        else:
            self.send_info(message="string generated " + str(len(self.move_generator.moves)) + " moves, best=" + best_move.to_pure())
            print("move", best_move.to_pure())

        # cleanup
        self.move_generator.moves = []

    def make_move(self, any_notation: str, color: str):
        """Register a given move to the internal single board by using internal board information."""
        self.send_info("string moving " + any_notation)

        if Move.is_pure_notation(any_notation=any_notation):
            piece = self.board.get_piece_at(any_notation[:2])
            is_capture = self.board.get_piece_at(any_notation[2:4]) != ""
            move = Move.from_pure(pure=any_notation, color=color, piece=piece, capture=is_capture)
        else:
            move = Move(san=any_notation, color=color)

        # care about castling moves like "Ke1g1" from UCI
        if move.piece.upper() == "K":  # noqa: SIM102
            if chess_distance(position1=move.from_position, position2=move.to_position) > 1:  # noqa: SIM102
                if move.from_position[0].lower() == "e":  # noqa: SIM102
                    if move.to_position[0].lower() == "g":
                        move.is_kingside_castling = True
                    elif move.to_position[0].lower() == "c":
                        move.is_queenside_castling = True
                    else:
                        raise AttributeError("cannot move " + move)

        self.board.make_move(move)


class UCI(Cmd):
    """UCI communication protocol implementation."""

    engine: Engine

    def __init__(self, engine: Engine):
        """Initialize UCI protocol adapter."""
        super().__init__()
        self.prompt = ""
        self.engine = engine

    def default(self, line: str) -> bool:
        """Empty because according to UCI any unknown command must be ignored."""
        return False

    def _debug(self, message: str):
        """Send message to GUI if debugging is enabled."""
        if self.engine.debug:
            print("info string", message)

    def precmd(self, line: str) -> str:
        """Hook in _debug() call for any call."""
        self._debug("got " + line)
        return line

    def do_uci(self, line: str):
        """First command indicating UCI protocol, we send appropriate answer."""
        print("id name", PROGRAM_NAME, VERSION)
        print("id author paruwo")
        # print('option name Hash type spin default 0 min 0 max 128')
        # print('setoption name UCI_AnalyseMode value false')
        print("uciok")

    def do_debug(self, line: str):
        """Enable UCI debug mode using 'info <message>' communication. We do not enable engine's debug mode though."""
        if "on" in line:
            self.engine.debug = True
            self._debug("debug mode enabled")
        elif "off" in line:
            self.engine.debug = False

    def do_isready(self, line: str):
        """GUI waits for the engine to finish initializing."""
        print("readyok")

    def do_setoption(self, line: str):
        """Set parameters as offered in do_uci()."""

    def do_registry(self, line: str):
        """Not implemented."""

    def do_ucinewgame(self, line: str):
        """GUI tells us that it starts a new game."""

    def do_position(self, line: str):
        """A complex call to tell the engine the position to start search at."""
        # position [fen <fenstring> | startpos ]  moves <move1> .... <movei>
        # position startpos moves e2e4cd w
        match = uci_positions_re.match(line)
        if not match:
            #
            self._debug('did not understand position "' + line + '"')

        if "fen " in line:
            fen_pos = line.find("fen ")
            moves_pos = line.find("moves ")
            fen_string = line[3 + fen_pos : (moves_pos - fen_pos)].strip()
            self.engine.setup_board(fen=fen_string)
        elif "startpos" in line:
            self.engine.setup_board()

        if "moves " in line:
            # TODO named regex groups
            moves = match.group(5).split(" ")
            # eliminate empty
            moves.remove("")

            color = self.engine.board.side_to_move
            for move in moves:
                # self._debug('applying ply ' + move + ' for ' + color)
                self.engine.make_move(any_notation=move, color=color)
                color = "w" if color == "b" else "b"

    def do_go(self, line: str):
        """GUI tells us to start searching best next move with respect to given parameters."""
        # go wtime 122000 btime 120000 winc 2000 binc 2000 depth 1
        if "wtime" in line:
            # self.engine.set_time_left(color='white', time=)
            pass
        if "btime" in line:
            pass
        if "depth" in line:
            pass

        # inherits color from move counting
        self.engine.react()

    def do_stop(self, line: str):
        """GUI dictates to stop searching and to return the 'best move' as fast as possible."""
        # hand 'stop' to engine and send 'bestmove <move>' back to gui

    def do_ponderhit(self, line: str):
        """?"""

    def do_quit(self, line: str):
        """Stop the engine as fast as possible."""
        self._debug("goodbye from pmc :)")
        sys.exit(0)


class CECP(Cmd):
    """
    Implementation of CECP / Winboard protocol.

    Minimal command set from GUI we need to react on include level, new, force,
    go and <MOVE> which fulfils pure coordinate notation.
    Minimal command we (the engine) have to send is <MOVE>.
    """

    engine: Engine
    v2_features_status: dict

    V2_FEATURES = {
        # ESSENTIAL commands
        "sigint": 0,
        "sigterm": 0,
        "setboard": 1,
        "ping": 1,
        "myname": f'"{PROGRAM_NAME} {VERSION}"',
        "memory": 0,
        "smp": 0,
        "egt": '""',
        # EXTENDED commands
        "reuse": 0,
        "usermove": 1,
        "debug": 1,
        "draw": 0,
        "pause": 0,
        "nps": 0,
        "variants": '"normal"',
        #
        "analyze": 0,
        "exclude": 0,
        # 'setscore': 0,
        # further standards to support, do not change
        "playother": 0,
        "ics": 0,
        "colors": 1,  # black/white must be supported but is obsolete since "setboard"
        "times": 1,
        "san": 0,
    }

    def __init__(self, engine: Engine):
        super().__init__()
        self.prompt = ""
        self.engine = engine
        self.v2_features_status = {}

    def _debug(self, message: str):
        """Internal method to hand message to GUI in case that debugging is enabled."""
        if self.engine.debug:
            print("#", message)

    def send_features(self):
        """..."""
        # TODO wait until "accepted"?
        for feature in self.V2_FEATURES:
            print(f"feature {feature}={self.V2_FEATURES[feature]}")

    def precmd(self, line: str) -> str:
        """Hook in _debug() call for any call."""
        self._debug("got " + line)
        return line

    def default(self, line: str):
        """Distinguish further actions from commands that cannot be reflected by CMD."""
        if line == "?":
            # TODO time is up, do move immediately
            pass

    # ############### Handshake

    def do_xboard(self, line: str):
        """Nothing to do, we know that it is CECP."""

    def do_protover(self, line: str):
        """Check that we cooperate with V2 engine."""
        if line != "2":
            print(f'# PROTOCOL ERROR for "{line}"')
            return

        print("feature done=0")
        self.send_features()
        print("feature done=1")

    def do_ping(self, line: str):
        """Enable ping/pong heartbeat. Needs ping=1."""
        print("pong", line)

    def do_accepted(self, line: str):
        """React if feature request was accepted by GUI."""
        self.v2_features_status[line] = True

    def do_rejected(self, line: str):
        """Read if feature request was rejected by GUI."""
        self.v2_features_status[line] = False

    # ############### BASIC DIALOG

    # TODO We should ignore commands black / white but Winboard uses them.

    def do_black(self, line: str):
        """Choose black side."""

    def do_white(self, line: str):
        """Choose white side."""

    def do_quit(self, line: str):
        """Stop all actions immediately and exit."""

    def do_new(self, line: str):
        """Start a new game in engine as black color."""
        self.engine.setup_board()

    def do_force(self, line: str):
        """Accept moves for any side, stop any search."""

    def do_go(self, line: str):
        """Start engine with current configuration."""
        self.engine.react()

    def do_usermove(self, line: str):
        """Make given move but demands "usermove" prefix. Needs usermove=1."""
        piece = self.engine.board.get_piece_at(position=line)
        color = "w" if piece.isupper() else "b"
        self.engine.make_move(any_notation=line, color=color)
        self._debug("making move to " + line + " with " + piece + " (" + color + ")")

        # accept opponents move and react
        self.engine.react()

    def do_setboard(self, line: str):
        """???"""

    def do_draw(self, line: str):
        """Opponent offers a draw, needs draw=1."""
        # if to accept: "offer draw"

    def do_result(self, line: str):
        """GUI sends final results."""
        # TODO parse for results as 1-0 or 0-1 or 1/2-1/2

    def do_undo(self, line: str):
        """???. See remove."""

    def do_remove(self, line: str):
        """???. See undo."""

    # ############### TIME & DEPTH CONTROL

    def do_level(self, line: str):
        """Level MOVESPERSESSION BASETIME INCREMENT."""

    def do_time(self, line: str):
        """Specify time left on clock in centi-seconds, needs time=1."""

    def do_otim(self, line: str):
        """Specify opponent's time left on clock in centi-seconds, needs time=1."""

    # ###### ENGINE PARAMETERS

    def do_memory(self, line: str):
        """Set MB of memory to use. Needs memory=1."""

    def do_cores(self, line: str):
        """Allow as many threads. Needs smp=1."""

    def do_egtpath(self, line: str):
        """Set path to EGT tablebase."""

    def do_random(self, line: str):
        """Enable randomization in engine."""

    def do_post(self, line: str):
        """Enable printing of 'Thinking Output' during the engine search."""

    def do_nopost(self, line: str):
        """Disable printing of 'Thinking Output' during the engine search."""

    def do_hard(self, line: str):
        """Enable pondering."""

    def do_easy(self, line: str):
        """Disable pondering."""


class Client:
    """Frontend class for direct user or machine interaction."""

    log: Logger

    def __init__(self):
        """Initialize the customer-facing client."""
        coloredlogs.install(milliseconds=True, level=logging.WARNING)
        self._parse_arguments()

    def _parse_arguments(self):
        """Set up, read and evaluate program parameters."""
        parser = argparse.ArgumentParser(description="Another simple chess engine.")
        parser.add_argument("-v", "--version", action="version", version=VERSION)
        parser.add_argument("-u", "--uci", dest="uci", default=False, action="store_true", help="Use UCI protocol for interaction to GUIs.")
        parser.add_argument(
            "-x",
            "--cecp",
            dest="cecp",
            default=False,
            action="store_true",
            help="Use XBoard/CECP/Winboard protocol for interaction to GUIs.",
        )
        parser.add_argument("--debug", dest="debug", default=False, action="store_true", help="Switch to debug level engine output.")
        args = parser.parse_args()

        if args.debug:
            coloredlogs.set_level(logging.DEBUG)

        if args.uci:
            engine = Engine(protocol="uci")
            UCI(engine=engine).cmdloop()
        elif args.cecp:
            engine = Engine(protocol="cecp")
            CECP(engine=engine).cmdloop()
        else:
            parser.print_help()


if __name__ == "__main__":
    Client()
