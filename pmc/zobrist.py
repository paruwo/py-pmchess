from random import randint, seed

from pmc.move import Move
from pmc.piece import Piece


class ZobristHash:
    """
    Implement board hashing to allow best move caching in transposition table.

    The idea is to keep a list of as many free positions as needed properties.
    Every position is filled with a (pseudo-) random value. Whenever we want to
    consider one of the properties, then we take that effective random value
    of the respective position and XOR it to the overall hash value.

    To unset a property (not consider the random value from its list position)
    then we XOR it again from the overall hash value.
    """

    __PIECE_INDEXES = {
        Piece.white_pawn.value: 1,
        Piece.white_bishop.value: 2,
        Piece.white_knight.value: 3,
        Piece.white_rook.value: 4,
        Piece.white_queen.value: 5,
        Piece.white_king.value: 6,
        #
        Piece.black_pawn.value: 7,
        Piece.black_bishop.value: 8,
        Piece.black_knight.value: 9,
        Piece.black_rook.value: 10,
        Piece.black_queen.value: 11,
        Piece.black_king.value: 12,
    }

    __EP_FILE_INDEXES = {
        "A": 0,
        "B": 1,
        "C": 2,
        "D": 3,
        "E": 4,
        "F": 5,
        "G": 6,
        "H": 7,
    }

    # keep position for the black-to-move indicator
    __BLACK_INDEX = 64 * 12
    # positions for possible castling rights indicator
    __CASTLING_INDEX = __BLACK_INDEX + 1
    # positions for possible en-passant capture files
    __EP_INDEX = __CASTLING_INDEX + 4

    # the list of raw initial 64bit random values to be used for hashing
    __zobrist: list

    # current hash value
    hash_value: int

    def __init_random(self):
        """Initialize board hashing instance with fix seed value to allow reproducible results."""
        # 64 fields x 12 possible pieces + 1 black side to move + 4 nums for castling + 8 possible en-passant files
        self.__zobrist = [0] * (64 * 12 + 1 + 4 + 8)
        seed(a=12956488132819173823)

        for field in range(0, 64):
            for piece in range(0, 12):
                self.__zobrist[12 * field + piece] = randint(0, 2**64)  # noqa: S311

        # black side to move indication
        self.__zobrist[self.__BLACK_INDEX] = randint(0, 2**64)  # noqa: S311

        # castling rights indication, white/black KQkq
        for castling in range(0, 4):
            self.__zobrist[self.__CASTLING_INDEX + castling] = randint(0, 2**64)  # noqa: S311

        # possible en-passant move files, A-H
        for en_passant in range(0, 8):
            self.__zobrist[self.__EP_INDEX + en_passant] = randint(0, 2**64)  # noqa: S311

    def __init_hash_value(self, str_board: str, color: str, castling: str, en_passant_file: str):
        # set initial hash value for basic setup
        for i in range(0, 64):
            if (piece := str_board[i]) != ".":
                j = ZobristHash.__PIECE_INDEXES[piece]
                self.hash_value ^= self.__zobrist[12 * i + j - 1]

        # in case, do switch color to move to black
        if color == "b":
            self.hash_value ^= self.__zobrist[self.__BLACK_INDEX]

        # KQkq
        if "K" in castling:
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX]
        if "Q" in castling:
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX + 1]
        if "k" in castling:
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX + 2]
        if "q" in castling:
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX + 3]

        if en_passant_file and en_passant_file in self.__EP_FILE_INDEXES:
            self.hash_value ^= self.__zobrist[self.__EP_INDEX + self.__EP_FILE_INDEXES[en_passant_file]]

    def __init__(self, str_board: str = None, color: str = "w", castling: str = "KQkq", en_passant_file: str = None):
        """Initialize Zobrist hashing instance."""
        if str_board is None:
            str_board = "".join(["rnbqkbnr", "pppppppp", "........", "........", "........", "........", "PPPPPPPP", "RNBQKBNR"])
        self.hash_value = 0
        self.__init_random()
        self.__init_hash_value(str_board=str_board, color=color, castling=castling, en_passant_file=en_passant_file)

    def incremental_hash(self, move: Move):
        """Faster shortcut version of hash() to only consider affected fields of given move."""
        # remove piece from source field
        piece_index = self.__PIECE_INDEXES[move.piece]
        self.hash_value ^= self.__zobrist[12 * move.from_index + piece_index]

        # place piece on target field
        self.hash_value ^= self.__zobrist[12 * move.to_index + piece_index]

        # if capture, remove captured piece from target
        if move.is_capture and move.captured_piece != ".":
            captured_piece_index = self.__PIECE_INDEXES[move.captured_piece]
            self.hash_value ^= self.__zobrist[12 * move.to_index + captured_piece_index]

        # if white, set black to move
        if move.color == "b":
            self.hash_value ^= self.__zobrist[self.__BLACK_INDEX]

        # castling, same as in FEN, i.e. KQkq
        if move.is_kingside_castling:
            castling_index_offset = 0 if move.color == "w" else 2
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX + castling_index_offset]
        elif move.is_queenside_castling:
            castling_index_offset = 1 if move.color == "w" else 3
            self.hash_value ^= self.__zobrist[self.__CASTLING_INDEX + castling_index_offset]

        # TODO really?
        if move.is_ep_capture:
            from_file = move.from_position[0]
            self.hash_value ^= self.__zobrist[self.__EP_INDEX + self.__EP_FILE_INDEXES[from_file]]
