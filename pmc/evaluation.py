from dataclasses import dataclass

from pmc.board import Board
from pmc.move import Move
from pmc.move_generator import MoveGenerator

GAME_PHASES = {
    "unknown": 0,
    "opening": 1,
    "middle game": 2,
    "endgame": 3,
}

PIECE_VALUES = {
    "p": 100,
    "n": 330,
    "b": 350,
    "r": 500,
    "q": 900,
    "k": 15000,
    ".": 0,
}

# center marked wir higher values
CENTER_CONTROL_MAP = [
    0,
    1,
    2,
    3,
    3,
    2,
    1,
    0,
    1,
    3,
    4,
    5,
    5,
    4,
    3,
    1,
    2,
    4,
    6,
    7,
    7,
    6,
    4,
    2,
    3,
    5,
    7,
    8,
    8,
    7,
    5,
    3,
    3,
    5,
    7,
    8,
    8,
    7,
    5,
    3,
    2,
    4,
    6,
    7,
    7,
    6,
    4,
    2,
    1,
    3,
    4,
    5,
    5,
    4,
    3,
    1,
    0,
    1,
    2,
    3,
    3,
    2,
    1,
    0,
]

# center affinity per piece
CENTER_CONTROL_PIECES = {
    "p": 1,
    "r": 2,
    "b": 3,
    "n": 4,
    "q": 3,
    "k": -1,
}


# Develop officer pieces
# conquer the center
# place rooks on open files
# avoid backward and isolated pawns
# avoid exposed king
# pawns to promote
# pawns to protect each other
# keep bishops and knights as teams


@dataclass
class Result:
    """Class for keeping results of board evaluations."""

    color: str
    value: int = 0  # value of all pieces in centipawns
    center_control: int = 0
    mobility: int = 0  # all piece's mobility
    turbulence: int = 0  # number of possible captures

    def value_no_king(self) -> int:
        if self.value > PIECE_VALUES["k"]:
            return self.value - PIECE_VALUES["k"]
        return self.value


class Evaluator:
    @staticmethod
    def determine_phase(board: Board) -> int:
        """Opening phase until 8th full move."""
        if board.move_counter < 9:
            return GAME_PHASES["opening"]
        return 2

    @staticmethod
    def evaluate(board: Board, moves: list[Move] = None, colors: str = "wb") -> tuple[Result, Result]:
        white_result = Result(color="w")
        black_result = Result(color="b")

        for index, piece in enumerate(board.str_board):
            if "w" in colors and piece.isupper():
                white_result.value += PIECE_VALUES[piece.lower()]
                white_result.center_control += CENTER_CONTROL_MAP[index] * CENTER_CONTROL_PIECES[piece.lower()]
            elif "b" in colors and piece.islower():
                black_result.value += PIECE_VALUES[piece.lower()]
                black_result.center_control += CENTER_CONTROL_MAP[index] * CENTER_CONTROL_PIECES[piece.lower()]

        if moves:
            turbulence = sum([1 for m in moves if m.is_capture])
            if board.side_to_move == "w" and "w" in colors:
                white_result.mobility = len(moves)
                white_result.turbulence = turbulence
            elif board.side_to_move == "b" and "b" in colors:
                black_result.mobility = len(moves)
                black_result.turbulence = turbulence

        return white_result, black_result


if __name__ == "__main__":
    board = Board(fen="r1bqkb1r/ppp2ppp/2nppn2/8/4P3/2BPBQ2/PPP2PPP/RN2K1NR w KQkq - 0 4")
    mg = MoveGenerator(setup_board=board)
    mg.generate()

    phase = Evaluator.determine_phase(board=board)
    # print(phase)

    white_eval, black_eval = Evaluator.evaluate(board=board, moves=mg.moves)
    # print(white_eval, white_eval.value_no_king(), black_eval, black_eval.value_no_king())
