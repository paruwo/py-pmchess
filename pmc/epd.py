"""Module to work with EPD testsuite files."""

import logging
import re
from pathlib import Path
from time import time

import coloredlogs

from pmc.board import Board
from pmc.config import Config
from pmc.fen import Position
from pmc.move_generator import MoveGenerator

epd_config = re.compile("(?P<color>[wb]) (?P<castling>-|[KQkq]{1,4}) (?P<enpassant>-|[a-h][36])")
# best move operator
epd_op_best_move = re.compile("bm (?P<bm>[^;]+);")
# move to avoid
epd_op_avoid_move = re.compile("am (?P<am>[^;]+);")
# identifier operator, reference to source etc.
epd_op_id = re.compile('(id (?P<id>"[^"]+"));')


class Epd:
    """
    Wrapper around EPD files. See https://www.chessprogramming.org/Extended_Position_Description for more details.

    EPDs consist of strings like
    "r3k2r/pbn2ppp/8/1P1pP3/P1qP4/5B2/3Q1PPP/R3K2R w - - bm Be2; id "L.Kaufman CCR Test No.9";".
    The first part until (in the given example) before "bm" is board setup.
    Afterwards a sequence of operators including parameters occur, divided by ";", like
    next best move to take (bm) or offer a draw (draw_offer).
    """

    log: logging
    tests: list[dict]
    file_name: str
    num_tested: int
    num_matched: int
    start_timestamp_ms: int
    success_rate: float

    def __init__(self, file_path: Path):
        """Initialize Extended Position Description instance."""
        self.log = logging.getLogger(self.__class__.__name__)
        self.file_name = file_path.name
        self.tests = []
        self.num_tested = 0
        self.num_matched = 0
        self.success_rate = 0.0
        self.start_timestamp_ms = round(time() * 1000)

        self._parse_file(file_path=file_path)

    def _parse_file(self, file_path: Path):
        """Go to given file, interpret every line of tests and run engine against tests."""
        # TODO refactor
        # FIXME too complex
        if not file_path.exists():
            raise AttributeError(f"file {file_path} does not exist")

        with file_path.open(encoding="utf-8") as file_pointer:
            active_test = False
            for line in file_pointer:
                test_entry = {}

                # only makes any sense if we can parse position setup
                if (fen_pos := Position.parse(line)) is not None:
                    logging.debug("%s - FEN ok", file_path)
                    test_entry["fen_pos"] = fen_pos.fen
                else:
                    continue

                if (conf := epd_config.search(line)) is not None:
                    logging.debug("Config parsed %s", {conf})
                    test_entry["side_to_move"] = conf["color"]
                    test_entry["castling"] = conf["castling"]
                    test_entry["en_passant_square"] = conf["enpassant"]

                if (operator_bm := epd_op_best_move.search(line)) is not None:
                    logging.debug("bm found %s %s", operator_bm, operator_bm[1])
                    active_test = True
                    self.num_tested += 1
                    test_entry["bm"] = operator_bm["bm"]
                    best_move = test_entry["bm"]

                    # run move generator
                    move_generator = MoveGenerator(setup_board=Board(fen=test_entry["fen_pos"]))
                    move_generator.generate()
                    generated_move = move_generator.best_move().to_epd()

                    logging.debug("expected %s, generated %s", best_move, generated_move)
                    if best_move == generated_move:
                        self.num_matched += 1

                if (operator_am := epd_op_avoid_move.search(line)) is not None:
                    logging.debug("am found %s %s", operator_am, operator_am[1])
                    active_test = True
                    self.num_tested += 1
                    test_entry["am"] = operator_am["am"].replace("?", "").replace("!", "").replace("+", "")

                    # run move generator
                    move_generator = MoveGenerator(setup_board=Board(fen=test_entry["fen_pos"]))
                    move_generator.generate()
                    generated_move = move_generator.best_move().to_epd()

                    logging.debug("to avoid %s - generated %s", test_entry["am"], generated_move)
                    if test_entry["am"] != generated_move:
                        self.num_matched += 1

                if (operator_id := epd_op_id.search(line)) is not None:
                    logging.debug("id found")
                    test_entry["id"] = operator_id["id"].strip('"')

                if test_entry != {}:
                    self.tests.append(test_entry)

                if not active_test:
                    logging.info("unknown test entry in %s", file_path.name)

            self.diff_ms = round(time() * 1000) - self.start_timestamp_ms
            if self.num_matched == 0 or self.num_tested == 0:
                self.success_rate = 0
            else:
                self.success_rate = round(100 * self.num_matched / self.num_tested, 1)

            logging.info(
                f'found {len(self.tests)} tests in EPD tests suite "{self.file_name}" '
                f"with {self.num_matched}/{self.num_tested} = {self.success_rate}% "
                f"successful in {self.diff_ms} ms"
            )


if __name__ == "__main__":
    coloredlogs.install(level="INFO")

    num_tested = 0
    num_matched = 0
    runtime_ms = 0

    test_folder = Config().root_path / "resource" / "testsuites"
    for testfile in test_folder.iterdir():
        if testfile.suffix != ".epd":
            continue
        epd = Epd(file_path=testfile)
        num_tested += epd.num_tested
        num_matched += epd.num_matched
        runtime_ms += epd.diff_ms

    success_rate = 0 if num_matched == 0 or num_tested == 0 else round(100 * num_matched / num_tested, 1)
    logging.info(f"EPD test suite results: {num_matched}/{num_tested} = {success_rate}% successful in {runtime_ms} ms")
