"""Module placeholder for perft functionality."""

import argparse
import logging
from dataclasses import dataclass
from logging import Logger
from time import time

import coloredlogs

from pmc.board import Board
from pmc.config import VERSION
from pmc.move_generator import MoveGenerator


@dataclass
class PerftResult:
    """A lightweight data container that holds perft results."""

    nodes: int
    captures: int
    castles: int
    ep_captures: int
    promotions: int
    checks: int

    # checks
    # checkmates

    def __add__(self, other):
        """Implement ADD operation on perft results."""
        return PerftResult(
            nodes=self.nodes + other.nodes,
            captures=self.captures + other.captures,
            castles=self.castles + other.castles,
            ep_captures=self.ep_captures + other.ep_captures,
            promotions=self.promotions + other.promotions,
            checks=self.checks + other.checks,
        )


class Perft:
    """
    Perft is a special program to debug chess engines.

    For a given board setup it generates all possible moves across several plies as a full game tree.
    This can take long time and consume lots of resources, especially for non-optimized engines like
    this one, pmc. As a gold standard, check out the timings and results from any commercial engine
    or stockfish.
    Additionally find some results at https://www.chessprogramming.org/Perft_Results.
    """

    log: Logger
    board: Board

    def __init__(self, fen: str = None):
        """Set up a perft instance."""
        self.log = logging.getLogger()
        if fen is None:
            self.board = Board()
        else:
            self.board = Board(fen=fen)

    def _run_bulk(self, board: Board, depth: int, initial_depth: int, divide: bool) -> PerftResult:
        """Slightly faster because we no not need to execute tree leave moves."""
        move_generator = MoveGenerator(setup_board=board)
        move_generator.generate()

        # https://stackoverflow.com/questions/30730983/make-lru-cache-ignore-some-of-the-function-arguments

        if depth <= 1:
            return PerftResult(nodes=len(move_generator.moves), captures=0, castles=0, ep_captures=0, promotions=0, checks=0)

        res = PerftResult(nodes=0, captures=0, castles=0, ep_captures=0, promotions=0, checks=0)
        for move in move_generator.moves:
            move_generator.board.make_move(move)
            tmp_res = self._run_bulk(board=move_generator.board, depth=depth - 1, initial_depth=initial_depth, divide=divide)
            if divide and depth == initial_depth:
                self.log.info("%s: %s", str(move), str(tmp_res.nodes))
            res += tmp_res
            move_generator.board.unmake_move()

        return res

    def _run(self, board: Board, depth: int, initial_depth: int, divide: bool) -> tuple[PerftResult, dict]:
        """First recursive approach to generate perft results."""
        if depth == 0:
            return PerftResult(nodes=1, captures=0, castles=0, ep_captures=0, promotions=0, checks=0), {}

        move_generator = MoveGenerator(setup_board=board)
        move_generator.generate()

        captures = sum([1 for m in move_generator.moves if m.is_capture])
        castles = sum([1 for m in move_generator.moves if m.is_kingside_castling or m.is_queenside_castling])
        ep_captures = sum([1 for m in move_generator.moves if m.is_ep_capture])
        promotions = sum([1 for m in move_generator.moves if m.promotion_piece not in [None, ""]])
        # FIXME, should be 34 for depth=4
        # FIXME pawn capture must contain target pawn
        checks = sum([1 for m in move_generator.moves if m.is_check])
        # TODO add checkmate

        res = PerftResult(nodes=0, captures=captures, castles=castles, ep_captures=ep_captures, promotions=promotions, checks=checks)
        div_map = {}

        for move in move_generator.moves:
            move_generator.board.make_move(move)

            tmp_res, tmp_div_map = self._run(board=move_generator.board, depth=depth - 1, initial_depth=depth, divide=divide)
            if divide and depth == initial_depth:
                self.log.info("%s: %s", str(move), str(tmp_res.nodes))
                div_map |= tmp_div_map
                div_map[str(move)] = tmp_res.nodes
            res += tmp_res

            move_generator.board.unmake_move()

        return res, div_map

    def run_and_print(self, depth: int, divide: bool, bulk: bool = False):
        """Trigger perft and show results."""
        if divide:
            self.log.info("starting in divide mode")

        time_start_ms = round(time() * 1000)
        if bulk:
            perft_result = self._run_bulk(board=self.board, depth=depth, initial_depth=depth, divide=divide)
        else:
            perft_result, _ = self._run(board=self.board, depth=depth, initial_depth=depth, divide=divide)
        time_diff_ms = round(time() * 1000) - time_start_ms

        nps = round(perft_result.nodes / (time_diff_ms / 1000))
        if nps >= 1e6:
            nps_formatted = str(round(nps / 1e6)) + "M"
        elif nps >= 1e3:
            nps_formatted = str(round(nps / 1e3)) + "k"
        else:
            nps_formatted = str(nps)

        perft.log.warning("generation of %i moves took %s ms with %s NPS", perft_result.nodes, time_diff_ms, nps_formatted)
        if not bulk:
            perft.log.warning("found %i captures", perft_result.captures)
            perft.log.warning("found %i en-passant captures", perft_result.ep_captures)
            perft.log.warning("found %i castles", perft_result.castles)
            perft.log.warning("found %i pawn promotions", perft_result.promotions)
            perft.log.warning("found %i checks", perft_result.checks)


if __name__ == "__main__":
    # logging.disable(logging.DEBUG)
    coloredlogs.install(level=logging.WARNING)
    parser = argparse.ArgumentParser(description="Benchmarking and correctness app for pmc.")
    parser.add_argument("-v", "--version", action="version", version=VERSION)
    parser.add_argument(
        "-d",
        "--depth",
        dest="depth",
        choices=["1", "2", "3", "4", "5", "6"],
        default="3",
        help="depth level to calculate perft for, will long for numbers > 4",
    )
    parser.add_argument("--divide", dest="divide", action="store_true", default=False, help="show split number of nodes per subtree")
    parser.add_argument("-f", "--fen", dest="fen", help="initial board situation as FEN string")
    parser.add_argument(
        "-b", "--bulk", dest="bulk", action="store_true", default=False, help="run in bulk-counting mode, computes #nodes only"
    )
    args = parser.parse_args()

    if int(args.depth) > 4:
        logging.warning("Pmc perft is neither optimized nor 100%% correct, yet.")
        logging.warning("Starting pmc perft anyway for depth %s", args.depth)

    perft = Perft(fen=args.fen)
    perft.run_and_print(depth=int(args.depth), bulk=args.bulk, divide=args.divide)
