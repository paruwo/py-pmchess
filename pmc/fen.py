"""Module to wrap FEN positions."""

from __future__ import annotations

import logging
import re

fen_board_re: re = re.compile(
    r"^(?P<board>([prnbqk0-9]{1,8}/){7}[prnbqk0-9]{1,8}) (?P<color>[wb]) "
    r"(?P<castling>[kq]+|-) (?P<enpassant>([abcdegfh][36])|-)"
    r"( (?P<halfmoves>\d{1,5}) (?P<moves>\d{1,5}))?",
    flags=re.IGNORECASE,
)

FEN_INITIAL = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"


class Validator:
    """https://chess.stackexchange.com/questions/1482/how-do-you-know-when-a-fen-position-is-legal"""

    @staticmethod
    def validate_board(board_setup: str) -> bool:
        """Check that sum of empty fields and pieces is 8 per rank."""
        for row in board_setup.split("/"):
            if piece_empty_sum := sum([int(c) if c.isnumeric() else 1 for c in row]) != 8:
                logging.debug("wrong row sum %i, but should be 8", piece_empty_sum)
                return False

            # no consecutive numbers
            last_was_number = False
            for char in row:
                if char.isnumeric():
                    if last_was_number:
                        return False
                    last_was_number = True
                else:
                    last_was_number = False
        return True

    @staticmethod
    def validate_kings(board_setup: str) -> bool:
        """Check that there is exactly one king per side."""
        kings = sum([1 for k in board_setup if k == "K"]) + sum([1 for k in board_setup if k == "k"])
        if kings != 2:  # noqa: SIM103
            return False

        # TODO kings must have distance >=1

        return True

    @staticmethod
    def validate_pawns(board_setup: str) -> bool:
        """Complex pawn checks.
        Check that there are no more than 8 pawns per side.
        Check that there are no paawns in first or last rank (wrong start position or missing promosion).
        """
        # Check that there are no more than 8 pawns per side
        white_pawns = sum([1 for p in board_setup if p == "P"])
        black_pawns = sum([1 for p in board_setup if p == "p"])
        if white_pawns > 8 or black_pawns > 8:
            return False

        # Check that there are no paawns in first or last rank (wrong start position or missing promosion)
        ranks = board_setup.split("/")
        first_last_ranks = ranks[0] + ranks[-1]
        white_pawns = sum([1 for p in first_last_ranks if p == "P"])
        black_pawns = sum([1 for p in first_last_ranks if p == "p"])
        if white_pawns > 0 or black_pawns > 0:  # noqa: SIM103
            return False

        # TODO In the case of en passant square; see if it was legally created (e.g it must be on
        #  the x3 or x6 rank, there must be a pawn (from the correct color) in front of it, and
        #  the en passant square and the one behind it are empty).
        # TODO Prevent having more promoted pieces than missing pawns
        # TODO The pawn formation is possible to reach (e.g in case of multiple pawns in a single
        #  col, there must be enough enemy pieces missing to make that formation), here are some useful rules:
        #     1) it is impossible to have more than 6 pawns in a single file (column) (because pawns can't exist
        #     in the first and last ranks).
        #     2) the minimum number of enemy missing pieces to reach a multiple pawn in a single col
        #     B to G 2=1, 3=2, 4=4, 5=6, 6=9 ___ A and H 2=1, 3=3, 4=6, 5=10, 6=15, for example, if you see
        #     5 pawns in A or H, the other player must be missing at least 10 pieces from his 15 captureable pieces.
        #     3) if there are white pawns in a2 and a3, there can't legally be one in b2, and this
        #     idea can be further expanded to cover more possibilities.

        return True

    @staticmethod
    def validate(board_setup: str) -> bool:
        """Call all existing validators and return overall result."""
        if not Validator.validate_board(board_setup=board_setup):
            return False
        if not Validator.validate_kings(board_setup=board_setup):
            return False
        if not Validator.validate_pawns(board_setup=board_setup):  # noqa: SIM103
            return False

        # CHECK
        # TODO Non-active color is not in check.
        # check for color self.to_move, but cannot use board because of circular imports
        # TODO Active color is checked less than 3 times (triple check is impossible)
        #   in case of 2 that it is never pawn+(pawn, bishop, knight), bishop+bishop, knight+knight.

        # CASTLING
        # TODO If the king or rooks are not in their starting position; the castling ability for
        #  that side is lost (in the case of king, both are lost).

        # BISHOPS
        # TODO Look for bishops in the first and last ranks (rows) trapped by pawns that haven't moved, for example:
        #    a bishop (any color) trapped behind 3 pawns.
        #    a bishop trapped behind 2 non-enemy pawns (not by enemy pawns because we can reach that position by
        #    underpromoting pawns, however if we check the number of pawns and extra_pieces we could determine
        #    if this case is possible or not).

        # NON-JUMPERS
        # TODO If there are non-jumpers enemy pieces in between the king and rook and there are
        #  still some pawns without moving; check if these enemy pieces could have legally gotten in
        #  there. Also, ask yourself: was the king or rook needed to move to generate that position?
        #  (if yes, we need to make sure the castling abilities reflect this).
        # TODO If all 8 pawns are still in the starting position, all the non-jumpers must not have
        #  left their initial rank (also non-jumpers enemy pieces can't possibly have entered legally),
        #  there are other similar ideas, like if the white h-pawn moved once, the rooks should still
        #  be trapped inside the pawn formation, etc.

        # CLOCK
        # TODO In case of an en passant square, the half move clock must equal to 0.
        # TODO HalfMoves <= ((FullMoves-1)*2)+(if BlackToMove 1 else 0), the +1 or +0 depends on the side to move.
        # TODO The HalfMoves must be x >= 0 and the FullMoves x >= 1.
        # TODO If the HalfMove clock indicates that some reversible moves were played and you can't
        #  find any combination of reversible moves that could have produced this amount of Halfmoves
        #  (taking castling rights into account, forced moves, etc), example: a side with many pawns and
        #  a king with castling rights and a rook (the HalfMove clock should not have been able to
        #  increase for this side).

        return True


class Position:
    """
    The Forsyth-Edwards Notation (FEN) describes a chess position.

    Find more information at https://www.chessprogramming.org/Forsyth-Edwards_Notation.
    An example is "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2".
    """

    log: logging
    fen: str
    board_setup: str
    to_move: str
    castling: str
    en_passant_target: str
    half_move_clock: int
    move_counter: int

    def __init__(self, fen: str):
        """Set up FEN position from given string."""
        self.log = logging.getLogger(self.__class__.__name__)

        self.fen = fen
        parsed = fen_board_re.match(fen)
        # needed by self.validate()
        self.board_setup = parsed["board"]

        if not parsed:
            self.log.error('fundamentally corrupt FEN "%s"', fen)
            raise AttributeError
        if not self.validate():
            self.log.error('corrupt FEN "%s"', fen)
            raise AttributeError

        self.to_move = parsed["color"]
        self.castling = parsed["castling"]
        self.en_passant_target = parsed["enpassant"]
        self.half_move_clock = int(parsed["halfmoves"]) if parsed["halfmoves"] else 0
        self.move_counter = int(parsed["moves"]) if parsed["moves"] else 0

        self.log.debug('fen "%s" recognized', fen)

    def __repr__(self):
        """Return this position as FEN string."""
        return self.fen

    def validate(self) -> bool:
        """Advanced FEN falidation through Validator."""
        return Validator.validate(board_setup=self.board_setup)

    @staticmethod
    def parse(fen_str: str) -> Position | None:
        """Read out given FEN string and return an instance of FenPosition if all valid."""
        fen_substr = fen_board_re.search(fen_str)
        if fen_substr:
            return Position(fen=fen_substr[0])
        return None

    def flip(self) -> str:
        """Mirrors the board vertically along the horizontal axis between the 4th and 5th rank."""
        # FIXME too complex
        rotated_fen = ""

        # board setup
        for rank in self.board_setup.split("/"):
            new_rank = ""
            for piece in rank:
                if piece.isupper():
                    new_rank += piece.lower()
                elif piece.islower():
                    new_rank += piece.upper()
                else:
                    new_rank += piece
            rotated_fen = new_rank + "/" + rotated_fen
        rotated_fen = rotated_fen.rstrip("/")

        # side to move
        if self.to_move == "w":
            rotated_fen += " b "
        else:
            rotated_fen += " w "

        # castling
        if self.castling != "-":
            new_castling = ""
            if "k" in self.castling:
                new_castling += "K"
            if "q" in self.castling:
                new_castling += "Q"
            if "K" in self.castling:
                new_castling += "k"
            if "Q" in self.castling:
                new_castling += "q"
            rotated_fen += new_castling
        else:
            rotated_fen += "-"

        # en-passant
        if "3" in self.en_passant_target:
            rotated_fen += " " + self.en_passant_target.replace("3", "6")
        elif "6" in self.en_passant_target:
            rotated_fen += " " + self.en_passant_target.replace("6", "3")
        else:
            rotated_fen += " -"

        return rotated_fen
