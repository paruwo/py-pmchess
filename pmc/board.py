"""A chess board, containing classes Board."""

import logging
from math import floor

from pmc.fen import FEN_INITIAL, Position
from pmc.move import Move, index_to_position, position_to_index
from pmc.zobrist import ZobristHash

INITIAL_STR_BOARD = "".join(["rnbqkbnr", "pppppppp", "........", "........", "........", "........", "PPPPPPPP", "RNBQKBNR"])

INITIAL_STR_BOARD_UTF8 = "".join(["♜♞♝♛♚♝♞♜", "♟♟♟♟♟♟♟♟", "        ", "        ", "        ", "♙♙♙♙♙♙♙♙", "♖♘♗♕♔♗♘♖"])

EMPTY_FIELD = " "


class Board:
    """Represent the central chess board."""

    # board-centric
    str_board: list
    moves: list[Move]
    castling: str
    en_passant_target: str
    fen: str
    halfmove_clock: int
    move_counter: int
    side_to_move: str
    hasher: ZobristHash

    # piece-centric bitboards
    # white_pawns: numpy.uint64
    # white_rooks: numpy.uint64
    # white_knights: numpy.uint64
    # white_bishops: numpy.uint64
    # white_queens: numpy.uint64
    # white_king: numpy.uint64

    # black_pawns: numpy.uint64
    # black_rooks: numpy.uint64
    # black_knights: numpy.uint64
    # black_bishops: numpy.uint64
    # black_queens: numpy.uint64
    # black_king: numpy.uint64

    def __setup_empty_board(self):
        logging.debug("setting up empty board")
        self.str_board = list(INITIAL_STR_BOARD)
        self.moves = []
        self.castling = "KQkq"
        self.en_passant_target = ""
        self.fen = FEN_INITIAL
        self.halfmove_clock = 0
        self.move_counter = 0
        self.side_to_move = "w"
        self.hasher = ZobristHash()

        # bitboards only needed for later fast bitwise move generator
        # self.white_pawns = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_11111111_00000000)
        # self.white_rooks = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_10000001)
        # self.white_knights = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_01000010)
        # self.white_bishops = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00100100)
        # self.white_queens = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00010000)
        # self.white_king = numpy.uint64(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00001000)

        # self.black_pawns = numpy.uint64(0b11111111_00000000_00000000_00000000_00000000_00000000_00000000_00000000)
        # self.black_rooks = numpy.uint64(0b10000001_00000000_00000000_00000000_00000000_00000000_00000000_00000000)
        # self.black_knights = numpy.uint64(0b01000010_00000000_00000000_00000000_00000000_00000000_00000000_00000000)
        # self.black_bishops = numpy.uint64(0b00100100_00000000_00000000_00000000_00000000_00000000_00000000_00000000)
        # self.black_queens = numpy.uint64(0b00001000_00000000_00000000_00000000_00000000_00000000_00000000_00000000)
        # self.black_king = numpy.uint64(0b00010000_00000000_00000000_00000000_00000000_00000000_00000000_00000000)

    def __setup_from_fen(self, fen: Position):
        logging.debug("got FEN string for board setup: %s", fen.board_setup)
        self.fen = fen.fen
        fen_board = ""

        for line in fen.board_setup.split(sep="/"):
            logging.debug("  line is %s", line)
            new_rank = ""
            for char in line:
                if char.isnumeric():
                    # uncompress spaces
                    new_rank += "." * int(char)
                else:
                    new_rank += char
            fen_board += new_rank

        self.castling = fen.castling
        self.en_passant_target = fen.en_passant_target
        self.halfmove_clock = fen.half_move_clock
        self.move_counter = fen.move_counter
        self.side_to_move = fen.to_move

        self.str_board = list(fen_board)
        self.moves = []
        self.hasher = ZobristHash(str_board=fen_board, color=self.side_to_move, castling=fen.castling)

    def __init__(self, fen: str = None):
        """Set up board initially or from a FEN string."""
        if fen is None:
            self.__setup_empty_board()
        else:
            fen_position = Position(fen=fen)
            self.__setup_from_fen(fen=fen_position)

    def get_piece_at(self, position: str) -> str:
        """Return the simple piece representation of current board setup."""
        return self.str_board[position_to_index(position=position)]

    def find_piece(self, piece: str) -> list[str]:
        """Search a given piece and return a list of position strings."""
        return [index_to_position(index=i) for i, p in enumerate(self.str_board) if p == piece]

    def last_moves_str(self, num_moves: int = 3) -> str:
        """Print the last N moves as groups of full moves."""
        moves_str = ""
        list_of_full_moves = [self.moves[i : i + num_moves - 1] for i in range(0, len(self.moves), num_moves - 1)]
        start_print = len(list_of_full_moves) - num_moves

        for current_move, full_moves in enumerate(list_of_full_moves):
            if current_move >= start_print:
                pair = " ".join(str(m) for m in full_moves)
                moves_str += "".join([str(current_move + 1), ". ", pair, "  "])

        return moves_str.strip()

    def __str__(self):
        """Return a human-readable version of current board."""
        repr_str = "    --------\n"
        for line in range(1, 9):
            repr_str += str(9 - line) + " | " + "".join(self.str_board[8 * (line - 1) : 8 * line])

            # ingest more information into remaining space
            if line == 1:
                repr_str += f"   {floor((1 + len(self.moves)) / 2)}. move, {self.side_to_move} to move"
            if line == 2 and len(self.moves) > 0:
                repr_str += f"   last move: {self.last_moves_str()}"
            elif line == 3:
                repr_str += f"   hash={str(self.hasher.hash_value)}"
            elif line == 7:
                repr_str += f"   fen is {self.get_fen()}"
            repr_str += "\n"
        repr_str += "    ________\n"
        repr_str += "    ABCDEFGH\n"
        return repr_str

    def get_piece_at_file(self, file: str, piece: str, color: str) -> str:
        """Retrieve piece position from given file only, makes sense for pawns only."""
        if color == "w":
            iterator_step = 1
            start_rank = 2
            end_rank = 9
            search_piece = piece.upper()
        else:
            iterator_step = -1
            start_rank = 8
            end_rank = 0
            search_piece = piece.lower()

        for rank in range(start_rank, end_rank, iterator_step):
            position = file + str(rank)
            logging.debug("searching %s at %s", search_piece, position)
            index = position_to_index(position=position)
            if self.str_board[index] == search_piece:
                logging.debug("found %s at %s", search_piece, position)
                return position

        logging.error(self)
        raise AttributeError(f"could not find {search_piece} at file {file} for color {color}")

    def make_move(self, move: Move):
        """Execute a board move."""
        # pawn moves may not have source information
        logging.info("moving %s", move)
        if move.move_type in (Move.PAWN_PUSH, Move.PAWN_CAPTURE):
            # moves like h6, search for Pawn in file H
            from_position = self.get_piece_at_file(file=move.san[0], piece=move.piece, color=move.color)
            move.set_from_position(from_position=from_position)
        elif len(move.from_position) != 2:
            # moves like Ngh6, search for Knight in file G
            from_position = self.get_piece_at_file(file=move.san[1], piece=move.piece, color=move.color)
            move.set_from_position(from_position=from_position)

        # fill up missing move information
        move.set_piece(self.str_board[move.from_index])
        move.set_captured_piece(piece=self.str_board[move.to_index])

        # the real move
        self.str_board[move.to_index] = self.str_board[move.from_index]
        self.str_board[move.from_index] = "."

        # additionally to the king, also move rook
        if move.is_kingside_castling:
            if move.color == "w":
                # "Ke1g1", for rook h1f1
                self.str_board[position_to_index(position="f1")] = "R"
                self.str_board[position_to_index(position="h1")] = "."
            else:
                self.str_board[position_to_index(position="f8")] = "r"
                self.str_board[position_to_index(position="h8")] = "."
        elif move.is_queenside_castling:
            if move.color == "w":
                # "Ke1c1", for rook a1d1
                self.str_board[position_to_index(position="d1")] = "R"
                self.str_board[position_to_index(position="a1")] = "."
            else:
                self.str_board[position_to_index(position="d8")] = "r"
                self.str_board[position_to_index(position="a8")] = "."

        if move.is_capture or move.piece.upper() == "P":
            self.halfmove_clock = 0
        else:
            self.halfmove_clock += 1

        if move.color == "b":
            self.move_counter += 1

        # switch color to move
        if self.side_to_move == "w":
            self.side_to_move = "b"
        else:
            self.side_to_move = "w"

        self.moves.append(move)
        self.hasher.incremental_hash(move=move)

    def unmake_move(self):
        """Undo last known move."""
        if len(self.moves) == 0:
            logging.warning("cannot unmake move, list of moves is empty")
            return
        move = self.moves.pop()
        logging.info("un-moving %s, len=%i", move, len(self.moves))

        self.str_board[move.from_index] = move.piece
        # if there is no real capture, the value is Piece.EMPTY.value == '.'
        self.str_board[move.to_index] = move.captured_piece

        if self.side_to_move == "w":
            self.side_to_move = "b"
        else:
            self.side_to_move = "w"

        if move.color == "w":
            self.move_counter -= 1

        # as already applied, if we apply it again we reset the state
        self.hasher.incremental_hash(move=move)

        # we cannot undo this...
        # if move.is_capture or move.piece.upper() == 'P':
        #    self.halfmove_clock = 0

    def flip(self):
        """Flip the current board piece-wise inplace and reset everything."""
        flipped_fen_str = Position(fen=self.fen).flip()
        flipped_fen = Position(fen=flipped_fen_str)
        self.__setup_from_fen(fen=flipped_fen)

    def get_fen(self) -> str:
        """Create first part of FEN string, not the complete - so 'w KQkq - 0 1' is missing for initial board setup."""
        fen_string = ""

        empty_fields = 0
        for pos, field in enumerate(self.str_board):
            if pos % 8 == 0:
                if empty_fields != 0:
                    fen_string += str(empty_fields)
                fen_string += "/"
                empty_fields = 0

            if field == ".":
                empty_fields += 1

            if field != ".":
                if empty_fields != 0:
                    fen_string += str(empty_fields)
                    empty_fields = 0
                fen_string += field

        self.fen = fen_string[1:]
        return fen_string[1:]
