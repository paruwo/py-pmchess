"""Module to wrap chess moves."""

import re

# non-pawn move, e.g. Qe2, Qe2xg2
from functools import cache
from typing import Optional

# SAN notation
# non-pawn move: Bc4, Qd1f3
san_piece_move_re: re = re.compile(r"^(?P<piece>[KQNBR])(?P<from>[a-h][1-8])?(?P<capture>x?)(?P<to>[a-h][1-8])?$", flags=re.IGNORECASE)
# pawn push: a2, aQ, a7N
san_pawn_push_re: re = re.compile(r"^(?P<to>[a-h][1-8])(?P<promotion>[KQNBR])?$", flags=re.IGNORECASE)
# pawn capture: axb5, a4xb5, axb8q, a7xb8q
san_pawn_capture_re: re = re.compile(r"^(?P<from>[a-h][1-8]?)x(?P<to>[a-h][1-8])(?P<promotion>[KQNBR])?$", flags=re.IGNORECASE)
# extended pawn push notation: a2a3

# LAN notation
# The leading piece symbol is optional.
# lan_pawn_push_re: re = re.compile(r'^([a-h][1-8])([x-])?([a-h][1-8])([KQRNB])?$', flags=re.IGNORECASE)
# lan_piece_move_re: re = re.compile(r'^([KQRNB])?([a-h][1-8])([x-])?([a-h][1-8])$', flags=re.IGNORECASE)

# pure LAN notation
# examples:  e2e4, e7e5, e1g1 (white short castling), e7e8q (for promotion)
pure_move_re: re = re.compile(r"^(?P<from>[a-h][1-8])(?P<to>[a-h][1-8])(?P<promotion>[KQRNB])?$", flags=re.IGNORECASE)

# EPD notation
epd_move_re: re = re.compile(
    r"^(?P<piece>[KQRNB])?(?P<from>[a-h][1-8])?(?P<action>[x-])?"
    r"(?P<to>[a-h][1-8])(?P<promotion>[KQRNB])?(?P<comment>[!?+]+)?$",
    flags=re.IGNORECASE,
)

move_position_index_mapping = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7}
MOVE_INDEX_POSITION_MAPPING = "abcdefgh"


@cache
def parse(position: str) -> bool:
    """Check if the given move can be interpreted."""
    return (
        san_piece_move_re.match(position) is not None
        or san_pawn_push_re.match(position) is not None
        or san_pawn_capture_re.match(position) is not None
    )


@cache
def position_to_index(position: str) -> int:
    """Map position string into postbox coordinates."""
    return move_position_index_mapping[position[0]] + 8 * (8 - int(position[1]))


@cache
def index_to_position(index: int) -> str:
    """Map postbox coordinates into position string."""
    return MOVE_INDEX_POSITION_MAPPING[(index % 8)] + str(8 - (index // 8))


class Move:
    """
    Represent a chess move to take.

    A simple standard format is used, e.g. a3, e5f6, a8q (promotion)
    """

    NON_PAWN = "NP"
    PAWN_PUSH = "PP"
    PAWN_CAPTURE = "CP"

    san: str
    lan: str
    pure: str
    from_position: str
    from_index: int
    to_position: str
    to_index: int
    piece: str
    color: str
    is_capture: bool
    is_kingside_castling: bool
    is_queenside_castling: bool
    captured_piece: str
    move_type: str
    is_double_pawn_push: bool
    is_ep_capture: bool
    promotion_piece: str
    is_check: bool

    @staticmethod
    def file_from_position(position: str) -> Optional[int]:
        """
        Return the vertical row = file from a given position string transformed into numerical index.

        A=0, B=1, ..., H=7.
        """
        try:
            return move_position_index_mapping[position[0]]
        except IndexError:
            return None

    @staticmethod
    def rank_from_position(position: str) -> Optional[int]:
        """Return the horizontal row = rank as numerical index from a given position string."""
        try:
            return int(position[1])
        except ValueError:
            return None

    @staticmethod
    def is_pure_notation(any_notation: str):
        """Check if given move in any notation is in pure notation."""
        return pure_move_re.match(any_notation)

    @classmethod
    def from_pure(cls, pure: str, color: str, piece: str, capture: bool):
        """Translate "pure" notation together with more information into SAN to get interpreted by Move."""
        if some_move := pure_move_re.match(pure):
            from_position = some_move["from"]
            to_position = some_move["to"]
            promotion_piece = some_move["promotion"] if some_move[3] else ""
            if piece.lower() in ["p", ""]:
                if capture:  # noqa: SIM108
                    # pawn capture like axb5, a4xb5, axb8q, a7xb8q
                    san = "".join([from_position, "x", to_position, promotion_piece])
                else:
                    # pawn push like a2, aQ, a7N
                    san = "".join([to_position, promotion_piece])
            else:
                capture_str = "x" if capture else ""
                san = "".join([piece, from_position, capture_str, to_position])
            return cls(san=san, color=color)

        raise ValueError("could not interpret given pure coordinate notation move " + pure)

    @classmethod
    def from_lan(cls, lan: str, color: str, from_position: str = None):
        """
        Create a move from given LAN move.

        As the move source could potentially be left away (e.g. in EPD test files) we have to include it as parameter.
        Examples are Rxe6, Ng4!, Qxh6?? or Bd4+.
        """
        if general_move := epd_move_re.match(lan):
            # Rxe6!
            parsed_piece = general_move["piece"]
            parsed_from = general_move["from"]
            parsed_action = general_move["action"]
            parsed_to = general_move["to"]
            parsed_promotion = general_move["promotion"]
            # parsed_comment = general_move['comment']

            if not parsed_from:
                parsed_from = from_position
            parsed_action = "x" if parsed_action == "x" else ""

            if parsed_promotion or not parsed_piece:
                # for normal pawn promotion we do not want the long form like "e7e8Q" but "e8Q" only
                if parsed_action == "":
                    parsed_from = ""
                san = "".join([parsed_from, parsed_action, parsed_to, parsed_promotion])
            else:
                san = "".join([parsed_piece, parsed_from, parsed_action, parsed_to])
        else:
            raise ValueError("could not interpret given LAN move " + lan)

        move = Move(san=san, color=color)
        if from_position:
            move.set_from_position(from_position=from_position)
        return move

    @staticmethod
    def from_uci(pure: str, color: str, piece: str, capture: bool):
        """Usually, UCI chess programs use shortened pure algebraic notation."""
        return Move.from_pure(pure=pure, color=color, piece=piece, capture=capture)

    def __init__(self, san: str, color: str):
        """Set up a move from SAN string and given color information."""
        # FIXME too complex!
        self.san = san
        self.lan = ""
        self.pure = ""
        self.color = color
        self.is_kingside_castling = False
        self.is_queenside_castling = False
        self.captured_piece = "."
        self.is_double_pawn_push = False
        self.is_ep_capture = False
        self.promotion_piece = ""
        self.is_check = False
        if non_pawn_move := san_piece_move_re.match(san):
            self.piece = non_pawn_move["piece"]
            self.from_position = non_pawn_move["from"].lower()
            self.is_capture = non_pawn_move["capture"] == "x"
            self.to_position = non_pawn_move["to"].lower()
            self.move_type = Move.NON_PAWN
            # castling
            if color == "w":
                if self.piece == "K" and self.from_position == "e1":
                    if self.to_position == "g1":
                        self.is_kingside_castling = True
                    if self.to_position == "c1":
                        self.is_queenside_castling = True
            else:
                if self.piece == "k" and self.from_position == "e8":
                    if self.to_position == "g8":
                        self.is_kingside_castling = True
                    if self.to_position == "c8":
                        self.is_queenside_castling = True
        elif pawn_push := san_pawn_push_re.match(san):
            self.piece = "p"
            self.is_capture = False
            self.to_position = pawn_push["to"].lower()
            # we know the right file
            self.from_position = self.to_position[0].lower()
            self.promotion_piece = pawn_push["promotion"]
            self.move_type = Move.PAWN_PUSH
            # TODO when is this condition reached?
            if self.to_position[1] == "4" and len(self.from_position) == 2 and self.from_position[1] == "2":
                self.is_double_pawn_push = True
        elif pawn_capture := san_pawn_capture_re.match(san):
            self.piece = "p"
            self.from_position = pawn_capture["from"].lower()
            self.is_capture = True
            self.to_position = pawn_capture["to"].lower()
            self.promotion_piece = pawn_capture["promotion"]
            self.move_type = Move.PAWN_CAPTURE
        else:
            raise AttributeError(f"invalid move {san}")

        # fix piece according to color, white=CAPITAL LETTER, black=small letter
        if color == "w":
            self.piece = self.piece.upper()
            if self.promotion_piece:
                self.promotion_piece = self.promotion_piece.upper()
        else:
            self.piece = self.piece.lower()
            if self.promotion_piece:
                self.promotion_piece = self.promotion_piece.lower()

        # we need file and rank to determine the position
        if len(self.from_position) == 2:
            self.from_index = position_to_index(self.from_position)
        else:
            self.from_index = -1

        if len(self.to_position) == 2:
            self.to_index = position_to_index(self.to_position)
        else:
            self.to_index = -1

    def __repr__(self):
        """Return this move in SAN."""
        return self.san + ("+" if self.is_check else "")

    def set_from_position(self, from_position: str):
        """Fill up potentially missing move source, like for pawn move e3."""
        self.from_position = from_position
        self.from_index = position_to_index(self.from_position)

        if self.piece == "P" and self.to_position[1] == "4" and self.from_position[1] == "2":
            self.is_double_pawn_push = True

    def set_captured_piece(self, piece: str):
        """Fill up missing information about which pieces was captured."""
        self.captured_piece = piece

    def set_piece(self, piece: str):
        """Fill up missing piece information."""
        if self.piece == "":
            self.piece = piece

    @staticmethod
    def is_sliding_piece(piece: str):
        """Check if given piece string reflects a sliding piece."""
        return piece.upper() in ["Q", "R", "B"]

    # @cached_property
    def to_lan(self) -> str:
        """
        Return the LAN representation of a chess move.

        See https://www.chessprogramming.org/Algebraic_Chess_Notation#LAN for more information.
        """
        if self.lan == "":
            action = "x" if self.is_capture else "-"
            if self.move_type == Move.NON_PAWN:
                self.lan = "".join([self.piece.upper(), self.from_position, action, self.to_position])
            else:
                self.lan = "".join(
                    [self.from_position, action, self.to_position, "" if self.promotion_piece is None else self.promotion_piece]
                )

        return self.lan

    # @cached_property
    def to_pure(self) -> str:
        """Return the pure algebraic representation of a chess move."""
        if self.pure == "":
            self.pure = "".join([self.from_position, self.to_position, ("" if self.promotion_piece is None else self.promotion_piece)])

        return self.pure

    def to_epd(self) -> str:
        """Give move representation as noted in EPD test files, e.g. h6, Nf6+, Qc8."""
        action = "x" if self.is_capture else ""
        check = "+" if self.is_check else ""
        if self.move_type == Move.NON_PAWN:
            return "".join([self.piece.upper(), action, self.to_position, check])

        # TODO how to properly express promotion which leads to check?
        promotion = "" if self.promotion_piece is None else self.promotion_piece
        return "".join([self.from_position, action, self.to_position, promotion, check])
