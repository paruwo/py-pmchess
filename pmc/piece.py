from enum import Enum


class Piece(Enum):
    EMPTY = "."

    white_king = "K"
    white_queen = "Q"
    white_rook = "R"
    white_bishop = "B"
    white_knight = "N"
    white_pawn = "P"

    black_king = "k"
    black_queen = "q"
    black_rook = "r"
    black_bishop = "b"
    black_knight = "n"
    black_pawn = "p"


# UTF8_PIECE_MAPPING = {
#    Piece.white_king: '♔',
#    Piece.white_queen: '♕',
# }
