"""
The move generator module, the brain of the chess engine.

We can start with Shannon's approach B [Shannon, 1950] to
play a game of perfect information where we can work with
heuristics. A more or less well-suited evaluation function
approximates the board setup and we can selectively follow
only those branches of the game tree that "fit our function".
Shannon's type A was pure brute-force and imaginary type C
would be based on "patterns" without raw computing power,
i.e. with ML techniques.

Basic chess strategies:
Develop officer pieces, conquer the center, place rooks on
open files, backward and isolated pawns to avoid as well as
exposed king, pawn to promote, pawns to protect each other,
keep bishops and knights as teams etc.

Interesting figures:
The number of possible chess positions is about exp(10, 43)
and the longest possible chess game is 6350 moves. Longest
tournament match until 1950 lasted 169 moves. A typical
position has about 30 possible moves and a typical game lasts
about 40 moves. Overall, we can calculate with about 1000
(~ 30*30) nodes for 2 plies.
"""

import logging
from abc import ABC, abstractmethod
from functools import cache
from random import choice
from time import time

from pmc.board import Board
from pmc.move import Move, index_to_position, move_position_index_mapping, position_to_index

# ray-wise movements, else hard to end sliding move
ROOK_MOVES = {
    "N": [+8, +16, +24, +32, +40, +48, +56],
    "E": [+1, +2, +3, +4, +5, +6, +7],
    "S": [-8, -16, -24, -32, -40, -48, -56],
    "W": [-1, -2, -3, -4, -5, -6, -7],
}
BISHOP_MOVES = {
    "NE": [+7, +14, +21, +28, +35, +42, +49],
    "SE": [+9, +18, +27, +36, +45, +54, +63],
    "SW": [-7, -14, -21, -28, -35, -42, -49],
    "NW": [-9, -18, -27, -36, -45, -54, -63],
}
KNIGHT_MOVES = [-15, -6, +10, +17, +15, +6, -10, -17]
# combination of rook and bishop move vectors
QUEEN_MOVES = {**BISHOP_MOVES, **ROOK_MOVES}
KING_MOVES = [-9, -8, -7, -1, +1, +7, +8, +9]


@cache
def invalid_position(position: str, previous_position: str = None) -> bool:
    """Execute basic check if move is inside of board."""
    file = Move.file_from_position(position)
    rank = Move.rank_from_position(position)

    if rank is None or file is None:
        return True

    if file < 0 or file > 7 or rank < 1 or rank > 8:
        return True

    if previous_position:
        # check of we break file (a3 -1 => h4) or rank
        previous_rank = Move.rank_from_position(previous_position)
        previous_file = Move.file_from_position(previous_position)
        return abs(previous_file - file) > 1 or abs(previous_rank - rank) > 1

    return False


@cache
def chess_distance(position1: str, position2: str) -> int:
    """Calculate the Chebyshev, the number of moves between both positions."""
    # max of (deviation of files, deviation of ranks)
    file1 = Move.file_from_position(position1)
    rank1 = Move.rank_from_position(position1)
    file2 = Move.file_from_position(position2)
    rank2 = Move.rank_from_position(position2)

    return max(abs(file2 - file1), abs(rank2 - rank1))


class GenericMoveGenerator(ABC):
    """An abstract wrapper for piece move generators."""

    @staticmethod
    @abstractmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """To be overwritten by concrete classes."""

    @staticmethod
    def is_opposite_color(piece: str, color: str):
        """Check if color of given piece has opposite color than the given one."""
        if color == "w":
            return piece.islower()
        return piece.isupper()

    @staticmethod
    def is_own_color(piece: str, color: str):
        """Check if color of given piece has the same color as the given one."""
        if color == "w":
            return piece.isupper()
        return piece.islower()

    @staticmethod
    def is_opponent(board: Board, target_position: str, reference_color: str) -> bool:
        """Check if piece at given position is of opponent color with respect to given color."""
        piece = board.get_piece_at(position=target_position)
        if reference_color == "w" and piece != ".":
            return piece.islower()
        if reference_color == "b" and piece != ".":
            return piece.isupper()
        return False


class KnightMoveGenerator(GenericMoveGenerator):
    """The move generator for knight pieces."""

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all valid moves for knight."""
        logging.debug("generating knight moves for %s", position)
        index = position_to_index(position=position)
        knight_moves = []

        for direction in KNIGHT_MOVES:
            target_index = index + direction
            target_position = index_to_position(index + direction)

            # we need another short check, else we create complete waste
            # for knights instead of MoveGenerator._invalid_position()
            if target_index < 0 or target_index > 63:
                continue
            if chess_distance(position, target_position) > 2:
                continue
            logging.debug("  checking move to %s", target_position)

            target_piece = board.get_piece_at(position=target_position)
            piece = "N" if color == "w" else "n"
            san = f"{piece}{position}"

            if target_piece == ".":
                logging.debug("  to empty %s", target_position)
                knight_moves.append(Move(san=san + target_position, color=color))
            elif GenericMoveGenerator.is_own_color(piece=target_piece, color=color):
                logging.debug("  to own piece at %s", target_position)
            elif GenericMoveGenerator.is_opposite_color(piece=target_piece, color=color):
                logging.debug("  to %s to capture %s", target_position, target_piece)
                knight_move = Move(san=san + "x" + target_position, color=color)
                knight_move.set_captured_piece(piece=target_piece)
                knight_moves.append(knight_move)

        return knight_moves


class RookMoveGenerator(GenericMoveGenerator):
    """The move generator for rook pieces."""

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all valid moves for rooks."""
        logging.debug("generating rook moves for %s", position)
        index = position_to_index(position=position)
        rook_moves = []

        for direction in ROOK_MOVES:
            logging.debug("  rook to move %s", direction)
            direction = ROOK_MOVES[direction]
            previous_target_position = None

            for rook_movement in direction:
                target_position = index_to_position(index + rook_movement)
                if previous_target_position is None:
                    previous_target_position = position

                logging.debug("  checking move to %s", target_position)

                if invalid_position(position=target_position, previous_position=previous_target_position):
                    logging.debug("  invalid move %s", target_position)
                    break
                previous_target_position = target_position

                target_piece = board.get_piece_at(position=target_position)
                piece = "R" if color == "w" else "r"
                san = f"{piece}{position}"

                if target_piece == ".":
                    logging.debug("  to empty %s", target_position)

                    move = Move(san=san + target_position, color=color)

                    # test for check
                    # TODO fixme
                    # board.make_move(move)
                    # is_king_under_attack = MoveGenerator.check_king_under_attack(board=board, color=opposite_color)
                    # board.unmake_move()

                    # m.is_check = is_king_under_attack
                    rook_moves.append(move)
                elif GenericMoveGenerator.is_own_color(piece=target_piece, color=color):
                    logging.debug("  to own piece at %s", target_position)
                    break
                elif GenericMoveGenerator.is_opposite_color(piece=target_piece, color=color):
                    logging.debug("  to %s to capture %s", target_position, target_piece)
                    rook_move = Move(san=san + "x" + target_position, color=color)
                    rook_move.set_captured_piece(piece=target_piece)
                    rook_moves.append(rook_move)
                    break

        return rook_moves


class KingMoveGenerator(GenericMoveGenerator):
    """The move generator for king pieces."""

    @staticmethod
    def check_king_under_ray_attack(board: Board, target_position: str, color: str) -> bool:
        """This method validates if the king of given color is under attack by generating the opponent's moves."""
        # diagonal attacks
        attack_moves = BishopMoveGenerator.generate(board=board, position=target_position, color=color)
        under_attack = any(1 for m in attack_moves if m.is_capture and m.captured_piece.upper() in ["B", "Q"])

        if under_attack:
            logging.debug("  king under diagonal ray attack")
            return True

        # horizontal / vertical attacks
        attack_moves = RookMoveGenerator.generate(board=board, position=target_position, color=color)
        under_attack = any(1 for m in attack_moves if m.is_capture and m.captured_piece.upper() in ["R", "Q"])
        if under_attack:
            logging.debug("  king under horizontal/vertical ray attack")
            return True

        return False

    @staticmethod
    def check_king_under_non_ray_attack(board: Board, target_position: str, color: str) -> bool:
        """Check if king is currently under any ray attack (by opponent sliding piece)."""
        # Knight
        attack_moves = KnightMoveGenerator.generate(board=board, position=target_position, color=color)
        under_attack = any(1 for m in attack_moves if m.is_capture and m.captured_piece.upper() == "N")
        if under_attack:
            logging.debug("  king under knight attack")
            return True

        # King - Chebyshev distance to king must be > 1
        logging.debug("determining distance to opponents king")
        opponent_king = "k" if color == "w" else "K"
        opponent_king_position = board.find_piece(piece=opponent_king).pop()
        under_attack = chess_distance(position1=target_position, position2=opponent_king_position) == 1
        if under_attack:
            logging.debug("  king under opponents king attack")
            return True

        # check for attacking Pawns
        logging.debug("determining opponents pawns attacks")
        if color == "w":
            opponent_pawn = "p"
            opponent_color = "b"
        else:
            opponent_pawn = "P"
            opponent_color = "w"
        opponent_pawn_positions = board.find_piece(piece=opponent_pawn)
        for attacking_pawn in opponent_pawn_positions:
            pawn_captures = PawnMoveGenerator.generate_possible_capture_moves(position=attacking_pawn, color=opponent_color)
            if target_position in pawn_captures:
                return True

        return False

    @staticmethod
    def check_king_castling(board: Board, color: str) -> bool:
        """Check that king-side short / castling is possible from chess board setup."""
        if color == "w":
            return (
                board.get_piece_at(position="e1") == "K"
                and board.get_piece_at(position="f1") == "."
                and board.get_piece_at(position="g1") == "."
                and board.get_piece_at(position="h1") == "R"
            )

        return (
            board.get_piece_at(position="e8") == "k"
            and board.get_piece_at(position="f8") == "."
            and board.get_piece_at(position="g8") == "."
            and board.get_piece_at(position="h8") == "r"
        )

    @staticmethod
    def check_queen_castling(board: Board, color: str) -> bool:
        """Check that queen-side / long castling is possible from chess board setup."""
        if color == "w":
            return (
                board.get_piece_at(position="e1") == "K"
                and board.get_piece_at(position="d1") == "."
                and board.get_piece_at(position="c1") == "."
                and board.get_piece_at(position="b1") == "."
                and board.get_piece_at(position="a1") == "R"
            )

        return (
            board.get_piece_at(position="e8") == "k"
            and board.get_piece_at(position="d8") == "."
            and board.get_piece_at(position="c8") == "."
            and board.get_piece_at(position="b8") == "."
            and board.get_piece_at(position="a8") == "r"
        )

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all valid moves for king."""
        logging.debug("generating king moves for %s", position)
        index = position_to_index(position=position)
        king_moves = []

        # TODO do not capture protected/backed pieces

        piece = "K" if color == "w" else "k"
        san = f"{piece}{position}"

        # regular moves
        for king_movement in KING_MOVES:
            target_position = index_to_position(index + king_movement)

            if invalid_position(position=target_position, previous_position=position):
                logging.debug("  invalid move %s", target_position)
                continue

            # check if king would be under any attack
            logging.debug("checking if %s king at %s is under attack", color, target_position)
            under_attack = KingMoveGenerator.check_king_under_ray_attack(board=board, target_position=target_position, color=color)
            under_attack |= KingMoveGenerator.check_king_under_non_ray_attack(board=board, target_position=target_position, color=color)
            if under_attack:
                logging.debug("  king would move into check at %s", target_position)
                continue

            target_piece = board.get_piece_at(position=target_position)

            if target_piece == ".":
                logging.debug("  to empty %s", target_position)
                king_moves.append(Move(san=san + target_position, color=color))
            elif GenericMoveGenerator.is_own_color(piece=target_piece, color=color):
                logging.debug("  to own piece at %s", target_position)
            elif GenericMoveGenerator.is_opposite_color(piece=target_piece, color=color):
                logging.debug("  to %s to capture %s", target_position, target_piece)
                king_move = Move(san=san + "x" + target_position, color=color)
                king_move.set_captured_piece(piece=target_piece)
                king_moves.append(king_move)

        # castling
        if color == "w":
            castling_rank = "1"
            can_kingside_castle = "K" in board.castling
            can_queenside_castle = "Q" in board.castling
        else:
            castling_rank = "8"
            can_kingside_castle = "k" in board.castling
            can_queenside_castle = "q" in board.castling

        if can_kingside_castle and KingMoveGenerator.check_king_castling(board=board, color=color):
            castling_move = Move(san=f"{piece}e{castling_rank}g{castling_rank}", color=color)
            king_moves.append(castling_move)
        if can_queenside_castle and KingMoveGenerator.check_queen_castling(board=board, color=color):
            castling_move = Move(san=f"{piece}e{castling_rank}c{castling_rank}", color=color)
            king_moves.append(castling_move)

        return king_moves


class BishopMoveGenerator(GenericMoveGenerator):
    """The move generator for bishop pieces."""

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all valid moves for bishop."""
        logging.debug("generating bishop moves for %s", position)
        index = position_to_index(position=position)
        bishop_moves = []

        for direction in BISHOP_MOVES:
            logging.debug("  bishop to move %s", direction)
            direction = BISHOP_MOVES[direction]
            previous_target_position = position

            for bishop_movement in direction:
                target_position = index_to_position(index + bishop_movement)
                logging.debug("  checking move to %s", target_position)

                if invalid_position(position=target_position, previous_position=previous_target_position):
                    logging.debug("  invalid move %s", target_position)
                    break

                target_piece = board.get_piece_at(position=target_position)
                piece = "B" if color == "w" else "b"
                san = f"{piece}{position}"
                previous_target_position = target_position

                if target_piece == ".":
                    logging.debug("  to empty %s", target_position)
                    m = Move(san=san + target_position, color=color)

                    # test for check
                    # FIXME debug make/unmake for bishop - this creates trouble running in perft
                    # logging.info('BISHOP')
                    # board.make_move(m)
                    # is_king_under_attack = MoveGenerator.check_king_under_attack(board=board, color=opposite_color)
                    # board.unmake_move()

                    # m.is_check = is_king_under_attack
                    bishop_moves.append(m)
                elif GenericMoveGenerator.is_own_color(piece=target_piece, color=color):
                    logging.debug("  to own piece at %s", target_position)
                    break
                elif GenericMoveGenerator.is_opposite_color(piece=target_piece, color=color):
                    logging.debug("  to %s to capture %s", target_position, target_piece)
                    bishop_move = Move(san=san + "x" + target_position, color=color)
                    bishop_move.set_captured_piece(piece=target_piece)
                    bishop_moves.append(bishop_move)
                    break

        return bishop_moves


class QueenMoveGenerator(GenericMoveGenerator):
    """The move generator for queen pieces."""

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all valid moves for queen."""
        logging.debug("generating queen moves for %s", position)
        index = position_to_index(position=position)
        queen_moves = []

        for direction in QUEEN_MOVES:
            logging.debug("  queen to move %s", direction)
            direction = QUEEN_MOVES[direction]
            previous_target_position = position

            for queen_movement in direction:
                target_position = index_to_position(index + queen_movement)
                logging.debug("  checking move to %s", target_position)

                if invalid_position(position=target_position, previous_position=previous_target_position):
                    logging.debug("  invalid move %s", target_position)
                    break

                target_piece = board.get_piece_at(position=target_position)
                if color == "w":
                    piece = "Q"
                    opposite_color = "b"
                else:
                    piece = "q"
                    opposite_color = "w"
                san = f"{piece}{position}"
                previous_target_position = target_position

                # move to empty field
                if target_piece == ".":
                    logging.debug("  to empty %s", target_position)
                    m = Move(san=san + target_position, color=color)

                    # test for check
                    logging.info("QUEEN")
                    board.make_move(m)
                    is_king_under_attack = MoveGenerator.check_king_under_attack(board=board, color=opposite_color)
                    board.unmake_move()

                    m.is_check = is_king_under_attack
                    queen_moves.append(m)
                    # continue to slide
                elif GenericMoveGenerator.is_own_color(piece=target_piece, color=color):
                    logging.debug("  to own piece at %s", target_position)
                    # end of sliding move
                    break
                elif GenericMoveGenerator.is_opposite_color(piece=target_piece, color=color):
                    # generate move
                    logging.debug("  to %s to capture %s", target_position, target_piece)
                    queen_move = Move(san=san + "x" + target_position, color=color)
                    queen_move.set_captured_piece(piece=target_piece)
                    queen_moves.append(queen_move)
                    # end of sliding move
                    break

        return queen_moves


class PawnMoveGenerator(GenericMoveGenerator):
    """The move generator for pawn pieces."""

    @staticmethod
    def _generate_promotion_moves(to_position: str, color: str) -> list[Move]:
        """Generate pawn promotion moves."""
        logging.debug("generating pawn promotion moves for %s", to_position)
        promotion_moves = []

        logging.debug("  pawn promotion to %s", to_position)
        if color == "w":
            queen_promotion = Move(san=to_position + "Q", color=color)
            rook_promotion = Move(san=to_position + "R", color=color)
            bishop_promotion = Move(san=to_position + "B", color=color)
            knight_promotion = Move(san=to_position + "N", color=color)
        else:
            queen_promotion = Move(san=to_position + "q", color=color)
            rook_promotion = Move(san=to_position + "r", color=color)
            bishop_promotion = Move(san=to_position + "b", color=color)
            knight_promotion = Move(san=to_position + "n", color=color)

        promotion_moves.append(queen_promotion)
        promotion_moves.append(rook_promotion)
        promotion_moves.append(bishop_promotion)
        promotion_moves.append(knight_promotion)

        return promotion_moves

    @staticmethod
    def generate_single_push_moves(board: Board, position: str, color: str) -> list[Move]:
        """Generate single push moves for pawns."""
        logging.debug("generating single push pawn moves for %s", position)
        index = position_to_index(position=position)
        rank = int(position[1])
        single_push_moves = []

        if color == "w":
            direction = -8  # move vector
            final_rank = 8  # last possible rank at board
            promotion_rank = 7  # rank where mandatory promotion must be done
            next_rank = +1
        else:
            direction = +8
            final_rank = 1
            promotion_rank = 2
            next_rank = -1

        if rank != final_rank and board.str_board[index + direction] == ".":
            to_position = position[0] + str(next_rank + int(position[1]))

            # single push at rank 7 is a mandatory promotion
            if rank == promotion_rank:
                single_push_moves.extend(PawnMoveGenerator._generate_promotion_moves(to_position=to_position, color=color))
            else:
                logging.debug("  moves to %s", to_position)
                single_push_moves.append(Move(san=to_position, color=color))

        return single_push_moves

    @staticmethod
    def generate_double_push_moves(board: Board, position: str, color: str) -> list[Move]:
        """Generate double push moves for pawns."""
        logging.debug("generating double push pawn moves for %s", position)
        index = position_to_index(position=position)
        rank = int(position[1])
        double_push_moves = []

        if color == "w":
            direction = -8  # move vector
            initial_rank = 2  # last possible rank at board
            double_push_rank = 2
        else:
            direction = +8
            initial_rank = 7
            double_push_rank = -2

        if rank == initial_rank:  # noqa: SIM102
            # single and double push fields must be empty
            if board.str_board[index + direction] == "." and board.str_board[index + 2 * direction] == ".":
                to_position = position[0] + str(double_push_rank + int(position[1]))
                logging.debug("  moves to %s", to_position)
                move = Move(san=to_position, color=color)
                move.is_double_pawn_push = True
                double_push_moves.append(move)

        return double_push_moves

    @staticmethod
    def generate_simple_capture_moves(board: Board, position: str, color: str) -> list[Move]:
        """Generate usual capturing moves for pawns."""
        logging.debug("generating usual capturing pawn moves for %s", position)
        index = position_to_index(position=position)
        capture_moves = []

        if color == "w":
            left_capture = -7
            right_capture = -9
        else:
            left_capture = +9
            right_capture = +7

        # simple capturing
        capture_left = index_to_position(index=index + left_capture)
        capture_right = index_to_position(index=index + right_capture)

        if capture_left[0] != "a" and PawnMoveGenerator.is_opponent(board=board, target_position=capture_left, reference_color=color):
            capture_left = position + "x" + capture_left
            logging.debug("  pawn captures at %s", capture_left)
            capture_moves.append(Move(san=capture_left, color=color))
        if capture_right[0] != "h" and PawnMoveGenerator.is_opponent(board=board, target_position=capture_right, reference_color=color):
            capture_right = position + "x" + capture_right
            logging.debug("  pawn captures at %s", capture_right)
            capture_moves.append(Move(san=capture_right, color=color))

        return capture_moves

    @staticmethod
    def generate_possible_capture_moves(position: str, color: str) -> list[str]:
        """Generate usual capturing target positions for pawns."""
        logging.debug("generating theoretical capturing pawn positions for %s", position)
        index = position_to_index(position=position)
        capture_positions = []

        if color == "w":
            left_capture = -7
            right_capture = -9
        else:
            left_capture = +9
            right_capture = +7

        capture_left = index_to_position(index=index + left_capture)
        capture_right = index_to_position(index=index + right_capture)

        if capture_left[0] != "a":
            capture_positions.append(capture_left)
        if capture_right[0] != "h":
            capture_positions.append(capture_right)

        logging.debug("pawn could theoretically move to %s", capture_positions)
        return capture_positions

    @staticmethod
    def generate_en_passant_capture(board: Board, position: str, color: str) -> list[Move]:
        """Generate en-passant capturing moves for pawns."""
        logging.debug("generating en-passant capturing pawn moves for %s", position)
        rank = int(position[1])
        ep_capture_moves = []

        if len(board.en_passant_target) == 2:
            if color == "w":
                ep_opponent_rank = "6"
                own_rank = 5
            else:
                ep_opponent_rank = "3"
                own_rank = 4

            logging.debug("  en-passant square set to %s", board.en_passant_target)
            opponent_file = board.en_passant_target[0]
            opponent_rank = board.en_passant_target[1]

            file_int = move_position_index_mapping[position[0]]
            opponent_file_int = move_position_index_mapping[board.en_passant_target[0]]

            # both pieces on same rank...
            if opponent_rank == ep_opponent_rank and rank == own_rank:  # noqa: SIM102
                # ... but on neighbour files
                if abs(file_int - opponent_file_int) == 1:
                    san = position + "x" + opponent_file + opponent_rank
                    logging.debug("  en-passant move detected %s", san)
                    move = Move(san=san, color=color)
                    move.is_ep_capture = True
                    ep_capture_moves.append(move)

        return ep_capture_moves

    @staticmethod
    def generate(board: Board, position: str, color: str) -> list[Move]:
        """Generate all pawn moves."""
        logging.debug("generating pawn moves for %s", position)
        pawn_moves = []

        pawn_moves.extend(PawnMoveGenerator.generate_single_push_moves(board=board, position=position, color=color))
        pawn_moves.extend(PawnMoveGenerator.generate_double_push_moves(board=board, position=position, color=color))
        pawn_moves.extend(PawnMoveGenerator.generate_simple_capture_moves(board=board, position=position, color=color))
        pawn_moves.extend(PawnMoveGenerator.generate_en_passant_capture(board=board, position=position, color=color))

        return pawn_moves


class MoveGenerator:
    """The main move generator for all pieces."""

    board: Board
    moves: list[Move]
    protocol: str
    deepening: bool
    is_king_under_attack: bool
    debug: bool

    def __init__(self, setup_board: Board, protocol: str = "uci"):
        """Feed MG with board information to base work on."""
        logging.debug("init move generator")
        self.board = setup_board
        self.moves = []
        self.protocol = protocol
        self.debug = False
        self.is_king_under_attack = False

    def send_info(self, message: str, protocol: str = "uci"):
        """Send further into to chess GUI, it's a quirks that this is not done in engine class."""
        if self.debug:
            if protocol == "uci":
                print("info", message)  # noqa: T201
            elif protocol == "cecp":
                print("#", message)  # noqa: T201

    @staticmethod
    def check_king_under_attack(board: Board, color: str) -> bool:
        """Check if king is under attack for the given color."""
        king = "K" if color == "w" else "k"
        king_position = board.find_piece(piece=king).pop()
        is_king_under_attack = KingMoveGenerator.check_king_under_ray_attack(board=board, target_position=king_position, color=color)
        is_king_under_attack |= KingMoveGenerator.check_king_under_non_ray_attack(board=board, target_position=king_position, color=color)
        return is_king_under_attack

    def generate(self, protocol: str = "") -> int:
        """Use the given board to generate all valid moves."""
        logging.debug("generating moves")
        if protocol != "":
            self.protocol = protocol
        self.send_info(message="depth 1 seldepth 1")

        color = self.board.side_to_move
        search_time_start = round(time() * 1000)
        iterator_step = 1

        self.is_king_under_attack = MoveGenerator.check_king_under_attack(board=self.board, color=color)
        # if self.is_king_under_attack:
        #    self.send_info(message='string recognized KING UNDER ATTACK')

        for file in ("a", "b", "c", "d", "e", "f", "g", "h"):
            for rank in range(1, 9, iterator_step):
                position = file + str(rank)
                piece = self.board.get_piece_at(position=position)

                if piece == ".":
                    continue
                if not GenericMoveGenerator.is_own_color(piece=piece, color=color):
                    continue

                piece_type = piece.upper()
                if piece_type == "P":
                    pawn_moves = PawnMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(pawn_moves)
                elif piece_type == "N":
                    knight_moves = KnightMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(knight_moves)
                elif piece_type == "R":
                    rook_moves = RookMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(rook_moves)
                elif piece_type == "B":
                    bishop_moves = BishopMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(bishop_moves)
                elif piece_type == "Q":
                    queen_moves = QueenMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(queen_moves)
                elif piece_type == "K":
                    king_moves = KingMoveGenerator.generate(board=self.board, position=position, color=color)
                    self.moves.extend(king_moves)

        search_timediff = round(time() * 1000) - search_time_start
        self.send_info(message="nodes " + str(len(self.moves)) + " time " + str(search_timediff))

        return len(self.moves)

    def best_move(self) -> Move:
        """Determine the best move with all possible moves given."""
        best_move = None
        logging.debug("starting to determine best move...")

        # if king is under attack, we have three options:
        # 1) move away king, 2) capture attacking pieces or 3) block ray attack with own piece
        if self.is_king_under_attack:
            logging.debug("1) king to move out of chess - not other options implemented so far")
            king_piece = "K" if self.board.side_to_move == "w" else "k"

            # 1) move away if there is an option
            if any(m for m in self.moves if m.piece == king_piece):
                king_move = choice([m for m in self.moves if m.piece == king_piece])
                if king_move:
                    logging.debug("best move is %s", king_move)
                    return king_move

            # TODO 2) capture attacking pieces
            # TODO 3) block ray attack with own piece

        for move in self.moves:
            if move.is_capture:
                best_move = move
                break
        logging.debug("2) selected any capture: %s", best_move)

        if not best_move:
            logging.debug("falling back to random")
            best_move = choice(self.moves)

        logging.debug("best move is %s", best_move)
        return best_move

    def get_num_castlings(self) -> int:
        """Return the number of castlings."""
        return sum([1 for m in self.moves if m.is_kingside_castling or m.is_queenside_castling])

    def get_num_captures(self) -> int:
        """Return the number of captures."""
        return sum([1 for m in self.moves if m.is_capture])

    def get_num_promotions(self) -> int:
        """Return the number of promotions."""
        return sum([1 for m in self.moves if m.promotion_piece != "."])

    def get_num_checks(self) -> int:
        """Return the number of checks."""
        return sum([1 for m in self.moves if m.is_check])
