"""An easy transposition table implemented as LRU table."""

from collections import OrderedDict
from typing import Optional


class TranspositionTable:
    """Taken and adapted from https://www.geeksforgeeks.org/lru-cache-in-python-using-ordereddict/."""

    capacity: int
    size: int

    def __init__(self, capacity_size: int = 0, capacity_mb: int = 0):
        """Initialize by capacity."""
        self.capacity = capacity_size
        self.size = 0
        if capacity_mb > 0:
            # apprx. that many entries we can keep per MB of OrderedDict
            self.capacity = capacity_mb * 6850

        if self.capacity > 0:
            self.cache = OrderedDict()

    def get(self, hash_value: int) -> Optional[str]:
        """Return cached value of None."""
        if self.capacity == 0:
            return None

        if hash_value not in self.cache:
            return None

        self.cache.move_to_end(hash_value)
        return self.cache[hash_value]

    def put(self, hash_value: int, best_move: str) -> None:
        """Store value for given key."""
        if self.capacity > 0:
            self.cache[hash_value] = best_move
            self.cache.move_to_end(hash_value)
            self.size += 1
            if len(self.cache) > self.capacity:
                self.cache.popitem(last=False)
                self.size -= 1

    def get_fill_state(self) -> int:
        """Fill state in permill."""
        if self.capacity > 0:
            return round(1000 * self.size / self.capacity)
        return 0
