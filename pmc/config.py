from pathlib import Path

from py_singleton import singleton

PROGRAM_NAME = "pmc"
VERSION = "0.1"


@singleton
class Config:
    """Central configuration instance."""

    root_path: Path

    def __init__(self):
        """Create instance that holds the configured values from configuration file."""
        self.root_path = Path(__file__).parent.parent
