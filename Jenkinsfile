#!/usr/bin/env groovy

// https://www.jenkins.io/doc/book/pipeline/syntax/

pipeline {
    agent {
        label('linux')
    }

    options {
        buildDiscarder logRotator(
            daysToKeepStr: '14',
            numToKeepStr: '10'
        )
    }

    stages {
        stage('Checkout') {
            steps {
                git url: 'https://codeberg.org/paruwo/py-pmchess.git'
            }
        }

        stage('Setup') {
            steps {
                withPythonEnv('python3.11') {
                    sh 'pip3 install poetry==1.8.3 && poetry install'
                }
            }
        }

        stage('Lint') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''poetry run ruff check'''
                }
            }
        }

        stage('Unit Tests') {
            steps {
                withPythonEnv('python3.11') {
                    sh 'poetry run pytest -n auto --junitxml=./unit-test-result.xml tests/unit'
                }
            }
            post {
                always {
                    junit 'unit-test-result.xml'
                }
            }
        }

        stage('Business Tests') {
            steps {
                withPythonEnv('python3.11') {
                    sh 'poetry run pytest tests/perft'
                }
            }
        }

        stage('Test Coverage') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''
                          poetry run coverage run --source=pmc/ -m pytest tests/unit
                          poetry run coverage xml -o unit-test-coverage.xml
                          poetry run coverage html
                       '''
                    }
            }
            post{
                always{
                    step([$class: 'CoberturaPublisher',
                           autoUpdateHealth: false,
                           autoUpdateStability: false,
                           coberturaReportFile: 'unit-test-coverage.xml',
                           failNoReports: false,
                           failUnhealthy: false,
                           failUnstable: false,
                           maxNumberOfBuilds: 10,
                           onlyStable: false,
                           sourceEncoding: 'ASCII',
                           zoomCoverageChart: false]
                    )
                }
            }
        }

        stage('Performance Tests') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''poetry run pytest \
                            --benchmark-autosave \
                            --benchmark-compare \
                            --benchmark-disable-gc \
                            --benchmark-name=short \
                            --benchmark-sort=name \
                            tests/performance
                    '''
                }
            }
        }

    }
}
